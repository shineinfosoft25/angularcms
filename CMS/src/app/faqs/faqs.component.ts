import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FAQ } from '../faq/faq.model';
import { FAQService } from '../faq/faq.service';


declare var $:any;
@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.component.html',
  styleUrls: ['./faqs.component.css']
})
export class FaqsComponent implements OnInit {
  FAQs= [];
  User : any;  
  pageNo: any = 1;  
  pageNumber: boolean[] = [];  
  sortOrder: any = 'FAQs';  
  pageField = [];  
  exactPageList: any;  
  paginationData: number;  
  UserPerPage: any = 5;  
  totalUsers: any;  
  totalUsersCount: any;
  constructor(public service: FAQService, public toastr:ToastrService,public router:Router,public route:ActivatedRoute) { 

    $(document).ready(function(){
      $("#container1").show();    
    }); 
  }

  ngOnInit(): void {   
      this.getAllFAQ();     
    }

  getAllFAQ() {    
    debugger;
      this.service.getAllFAQ(this.pageNo, this.UserPerPage, this.sortOrder).subscribe((data: any) => {  
        this.FAQs  = data.data.faqs;
        this. totalUsersCount = data.data.count;  
        this.totalNoOfPages();   
      })         
    } 

    onDelete(id)
    {
      if(confirm('Are you sure you want to delete this record?')){
        
        this.service.DeleteFAQ(id)
        .subscribe(
       (res:any) => {
        this.toastr.error('Deleted Successfully','Register Detail');
        this.getAllFAQ();
  
       },
          err => {
             
            if( err.status == 500 ) {
  
              this.toastr.error('Internal Server error');
            } else if( err.status == 400 ) {
        
              this.toastr.error('Unauthorize User');
            }
          });
  
      }  
    }  
    onEdit(item:FAQ){        
      this.router.navigate(['/faq',item.id]);  
    }    

    totalNoOfPages() {  
      this.paginationData = Number(this. totalUsersCount / this.UserPerPage);  
      let tempPageData = this.paginationData.toFixed();  
      if (Number(tempPageData) < this.paginationData) {  
        this.exactPageList = Number(tempPageData) + 1;  
        this.service.exactPageList = this.exactPageList;  
      } else {  
        this.exactPageList = Number(tempPageData);  
        this.service.exactPageList = this.exactPageList  
      }  
      this.service.pageOnLoad();  
      this.pageField = this.service.pageField;  
    }  
    
    showCompaniesByPageNumber(page, i) {  
      this.FAQs  = [];  
      this.pageNumber = [];  
      this.pageNumber[i] = true;  
      this.pageNo = page;  
      this.getAllFAQ();  
    }   
}
