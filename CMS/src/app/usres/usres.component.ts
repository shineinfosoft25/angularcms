import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from '../registration/api.service';


declare var $:any;
@Component({
  selector: 'app-usres',
  templateUrl: './usres.component.html',
  styleUrls: ['./usres.component.css']
})
export class UsresComponent implements OnInit {
  Users = [];
  User : any;  
  pageNo: any = 1;  
  pageNumber: boolean[] = [];  
  sortOrder: any = 'US';  
  pageField = [];  
  exactPageList: any;  
  paginationData: number;  
  UserPerPage: any = 5;  
  totalUsers: any;  
  totalUsersCount: any;
  constructor(public service: ApiService, public toastr:ToastrService,public router:Router,public route:ActivatedRoute) {    $(document).ready(function(){
    $("#container1").show();    
  });
  $("#active").hide();    

 }

ngOnInit(): void {

  this.getAllUser();

}

getAllUser() {
  debugger;  
    this.service.getAllUser(this.pageNo, this.UserPerPage, this.sortOrder).subscribe((data: any) => {  
      debugger;
      this.Users  = data.data.users;
      for (let index = 0; index < this.Users.length; index++) {
        this.Users [index].profile= 'http://192.168.1.68/UserThumb/'+ this.Users[index].profile;
      }
      this. totalUsersCount = data.data.count;  
      this.totalNoOfPages();       
    })       
  } 

  totalNoOfPages() {  
    this.paginationData = Number(this. totalUsersCount / this.UserPerPage);  
    let tempPageData = this.paginationData.toFixed();  
    if (Number(tempPageData) < this.paginationData) {  
      this.exactPageList = Number(tempPageData) + 1;  
      this.service.exactPageList = this.exactPageList;  
    } else {  
      this.exactPageList = Number(tempPageData);  
      this.service.exactPageList = this.exactPageList  
    }  
    this.service.pageOnLoad();  
    this.pageField = this.service.pageField;  
  }
  
  
  showCompaniesByPageNumber(page, i) {  
    this.Users  = [];  
    this.pageNumber = [];  
    this.pageNumber[i] = true;  
    this.pageNo = page;  
    this.getAllUser();  
  }  

onActive(id)
{
  if(confirm('Are you sure you want to Active this record?')){
    debugger;
    this.service.ActiveUser(id)
    .subscribe(
   (res:any) => {
    this.toastr.success('Active Successfully','Register Detail');
    this.getAllUser();
   },
      err => {
         
        if( err.status == 500 ) {

          this.toastr.error('Internal Server error');
        } else if( err.status == 400 ) {
    
          this.toastr.error('Unauthorize User');
        }
      });
  }  
}

  onDelete(id)
  {
     debugger;
   
     
      if(confirm('Are you sure you want to delete this record?')){
        debugger;
        this.service.DeleteUser(id)
        .subscribe(
       (res:any) => {
        this.toastr.error('Deleted Successfully','Register Detail');
        this.getAllUser();
       },
          err => {
             
            if( err.status == 500 ) {
  
              this.toastr.error('Internal Server error');
            } else if( err.status == 400 ) {
        
              this.toastr.error('Unauthorize User');
            }
          });
      
        }
  }
     
  }







