import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FAQService } from '../faq/faq.service';

declare var $:any;
@Component({
  selector: 'app-editfaq',
  templateUrl: './editfaq.component.html',
  styleUrls: ['./editfaq.component.css']
})
export class EditfaqComponent implements OnInit {

  constructor(public service:FAQService, public toastr:ToastrService,public router:Router,public route:ActivatedRoute,public http: HttpClient) { 

    $(document).ready(function(){
      $("#container1").show();    
    });
  }

  ngOnInit(): void {
    this.resetForm(); 
    this.route.paramMap.subscribe(res=>{ 
      const id = +res.get('id');     
      this.GetFAQByID(id);
    });
  }


  GetFAQByID(id) {
    debugger;
    this.service.GetFAQByID(id).subscribe((data: any) => {
      debugger; 
        this.service.FormData={
        id:data.data.id,
        Question:data.data.question,
        Answers:data.data.answers,
        createdBy:data.data.createdBy                   
      };   
    },
    )

  }


  

  resetForm(form?:NgForm){
    if(form!=null)
    form.resetForm();
    this.service.FormData={
      id:null,
      Question:'',
      Answers:'',
      createdBy:'',
    }
  }

  onSubmit(form:NgForm){
    debugger;
    if(this.service.FormData.id>0)
    {
      this.updateRecord(form);
    }
 
  }
  updateRecord(form:NgForm){
    this.service.EditFAQ().subscribe(
      res=>{
        this.toastr.success('Updated Successfully','Register Detail');
        this.resetForm(form);
        this.router.navigate(['/faqs']);
      },
      err => {
           
        if( err.status == 500 ) {
  
          this.toastr.error('Internal Server error');
        } else if( err.status == 400 ) {
    
          this.toastr.error('Unauthorize User');
        }
      }     
    ) 
}
}
