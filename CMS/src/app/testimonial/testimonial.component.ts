import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TestimonialService } from './testimonial.service';


declare var $:any;
@Component({
  selector: 'app-testimonial',
  templateUrl: './testimonial.component.html',
  styleUrls: ['./testimonial.component.css']
})
export class TestimonialComponent implements OnInit {
  imageUrl: string = "/assets/user.jpg";
  fileToUpload: File = null;
  constructor(public service:TestimonialService,
    public toastr:ToastrService,public router:Router,public route:ActivatedRoute,public http: HttpClient) {
   
      $(document).ready(function(){
        $("#container1").show();    
      }); 
     }

  ngOnInit(): void {
    this.resetForm();
  }

  resetForm(form?:NgForm){
    if(form!=null)
    form.resetForm();
    this.service.FormData={
      id:null,
      Name:'',
      Title:'',
      Description:'',
      Designation:'',  
      createdBy:'',   
      Image:'',
      Status:null
    }
  }
  
  
  handleFileInput(file:FileList)
  {
    this.fileToUpload = file.item(0);
    var reader = new FileReader();
    reader.onload =(event:any)=>{
    this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }   


onSubmit(form:NgForm){
  debugger;
  if(this.service.FormData.id==null)
  {
    this.insertRecord(form);
  }
}

insertRecord(form:NgForm){
  debugger;
  this.service.AddTestimonial(this.fileToUpload).subscribe(
    res=>{
      this.toastr.success('Submitted Successfully','Register Detail');
      this.resetForm(form);
      this.router.navigate(['/']);
      },
      err=>{
        console.log(err);       
    }
  )
}
}
