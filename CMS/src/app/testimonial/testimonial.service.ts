import { Injectable, Injector } from '@angular/core';  
import { HttpClient, HttpHeaders } from '@angular/common/http';  
import { Observable } from 'rxjs';  
import { ToastrService } from 'ngx-toastr';
import { Testimonial } from './testimonial.model';

@Injectable({  
  providedIn: 'root'  
})  
export class TestimonialService {
  FormData:Testimonial;
  readonly rootURL ='http://localhost:62921/api';

  temppage: number = 0;  
  pageField = [];  
  exactPageList: any;  
    private url = "";  
    constructor(public http: HttpClient,private injector:Injector,public toastr:ToastrService) {  
  }
    
    AddTestimonial(fileToUpload :File)
    {     
      const formData:FormData = new FormData();
      formData.append('Title',this.FormData.Title); 
      formData.append('Description',this.FormData.Description);
      formData.append('Name',this.FormData.Name);
      formData.append('Designation',this.FormData.Designation);   
      formData.append('createdBy',this.FormData.createdBy);
      formData.append('Image', fileToUpload, fileToUpload.name);
      return this.http.post(this.rootURL+'/testimonials',formData)
    }

    getAllTestimonial(pageNo,pageSize,sortOrder): Observable<any> {  
      const httpOptions = {
        headers: new HttpHeaders({  
        'Authorization': `Bearer ${sessionStorage.getItem('token')}`
        })
      };  
      this.url = 'http://192.168.1.68/api/testimonials?take='+pageSize+'&skip='+(pageNo-1)*pageSize;    
        return this.http.get(this.url);  
    }       

    GetTestimonialByID(id){     
        return this.http.get(this.rootURL+'/testimonials/'+ id)
      } 

      DeleteTestimonial(id){  
        return this.http.delete(this.rootURL+'/testimonials/'+ id)    
      }   

      EditTestimonial(fileToUpload: File,){
        debugger;   
        const formData: FormData = new FormData();
        formData.append('Title', this.FormData.Title);
        formData.append('Description', this.FormData.Description);
        formData.append('Name', this.FormData.Name);
        formData.append('Designation', this.FormData.Designation);
        formData.append('createdBy', this.FormData.createdBy);
        formData.append('Image', fileToUpload, fileToUpload.name)
        return this.http.patch(this.rootURL+'/testimonials/'+this.FormData.id,formData);  
      } 

      pageOnLoad() {       
        if (this.temppage == 0) {  
            this.pageField = [];  
            for (var a = 0; a < this.exactPageList; a++) {  
                this.pageField[a] = this.temppage + 1;  
                this.temppage = this.temppage + 1;  
          }  
        }  
    } 



}  