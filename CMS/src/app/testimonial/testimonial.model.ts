export class Testimonial {
    id:number;
    Title:string;
    Name:string;
    Description:string;
    Designation:string; 
    createdBy:string; 

    Image:string="/assests/Image";
    Status:number;
 }
 export class Datum {
    id:number;
    Title:string;
    Description:string;
    Designation:string;  
    createdBy:string; 
    Image:string;
    Status:number;
}
export interface RootObject {
    httpStatusCode: number;
    errorCode: number;
    message: string;
    data: Datum[];
}
