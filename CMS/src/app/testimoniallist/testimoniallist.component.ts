import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Testimonial } from '../testimonial/testimonial.model';
import { TestimonialService } from '../testimonial/testimonial.service';
import 'bootstrap/dist/css/bootstrap.min.css';

'use strict';
declare var $:any;
@Component({
  selector: 'app-testimoniallist',
  templateUrl: './testimoniallist.component.html',
  styleUrls: ['./testimoniallist.component.css']
})
export class TestimoniallistComponent implements OnInit {
  Testimonial = [];
  User : any;  
  pageNo: any = 1;  
  pageNumber: boolean[] = [];  
  sortOrder: any = 'TestimonialList';  
  pageField = [];  
  exactPageList: any;  
  paginationData: number;  
  UserPerPage: any = 5;  
  totalUsers: any;  
  totalUsersCount: any;
  
  constructor(public service: TestimonialService, public toastr:ToastrService,public router:Router,public route:ActivatedRoute) { 

      $(document).ready(function(){
        $("#container1").show();    
      });

  }

  ngOnInit(): void {
    // this.pageNumber[0] = true;      
    // this.getAllTestimonial();    


    if(sessionStorage.getItem('token')){  
      debugger;
      this.pageNumber[0] = true; 
      
      this.getAllTestimonial();  
      }
    
    else
    {
      this.router.navigate(['/']);
    }
  }

  getAllTestimonial() {
    // angular.module('myApp', [require('angular-sessionstorage')])
        debugger;  
      this.service.getAllTestimonial(this.pageNo, this.UserPerPage, this.sortOrder).subscribe((data: any) => {  
        this.Testimonial = data.data.testimonials;
        for (let index = 0; index < this.Testimonial.length; index++) {
          this.Testimonial[index].image= 'http://192.168.1.68/ImagesPath/'+ this.Testimonial[index].image;
        }
        this. totalUsersCount = data.data.count;  
        this.totalNoOfPages();        
      })
    }  

    Logout() {
      this.toastr.success('Logout Successfully.')
      this.router.navigate(['/']);
    } 

    onDelete(id)
    {
      if(confirm('Are you sure you want to delete this record?')){
        debugger;
        this.service.DeleteTestimonial(id)
        .subscribe(
       (res:any) => {
        this.toastr.error('Deleted Successfully','Register Detail');
        this.getAllTestimonial();  
       },
          err => {             
            if( err.status == 500 ) {  
              this.toastr.error('Internal Server error');
            } else if( err.status == 400 ) {
                      this.toastr.error('Unauthorize User');
            }
          }); 
        }  
    }  
    onEdit(item:Testimonial){      
      this.router.navigate(['/testimonial',item.id]); 
    }   
    
    totalNoOfPages() {  
      this.paginationData = Number(this. totalUsersCount / this.UserPerPage);  
      let tempPageData = this.paginationData.toFixed();  
      if (Number(tempPageData) < this.paginationData) {  
        this.exactPageList = Number(tempPageData) + 1;  
        this.service.exactPageList = this.exactPageList;  
      } else {  
        this.exactPageList = Number(tempPageData);  
        this.service.exactPageList = this.exactPageList  
      }  
      this.service.pageOnLoad();  
      this.pageField = this.service.pageField;  
    }  
    showCompaniesByPageNumber(page, i) {  
      this.Testimonial  = [];  
      this.pageNumber = [];  
      this.pageNumber[i] = true;  
      this.pageNo = page;  
      this.getAllTestimonial();  
    }   
  }
