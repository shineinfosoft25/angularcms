import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from '../registration/api.service';


@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.css']
})
export class EdituserComponent implements OnInit {
  imageUrl: string = "/assets/Image/user.jpg";
  readonly rootURL ='http://192.168.1.68/api';
  fileToUpload: File = null;
  constructor(public service:ApiService,
    public toastr:ToastrService,public router:Router,public route:ActivatedRoute,public http: HttpClient) { }

  ngOnInit(): void {
    if(localStorage.getItem('LoginStatus')=='1'){  
    this.resetForm();
  
  }
    else
  {
    this.router.navigate(['/']);
  }

  }
  GetUserByID(id) {
    debugger;
    localStorage.setItem('LoginStatus','1');
    this.service.GetUserByID(id).subscribe((data: any) => {
      debugger;
        this.imageUrl=  "http://192.168.1.68/UserThumb/"+data.data.profile;
        this.service.FormData={
        id:data.data.id,
        UserName:data.data.userName,
        Email:data.data.email,
        Password:data.data.password,
        Profile:data.data.profile,   
        UserType:data.data.UserType,        
        Status:1,
      };
   
    
    },
    err => {
           
      if( err.status == 500 ) {

        this.toastr.error('Internal Server error');
      } else if( err.status == 400 ) {
  
        this.toastr.error('Unauthorize User');
      }
    }
    )
  

  }




 
  
  handleFileInput(file:FileList)
  {
    this.fileToUpload = file.item(0);
    var reader = new FileReader();
    reader.onload =(event:any)=>{
    this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }
  
  resetForm(form?:NgForm){
    if(form!=null)
    form.resetForm();
    this.service.FormData={
      id:null,
      UserName:'',
      Email:'',
      Password:'',
      Profile:'',
      UserType:'',
      Status:null
    }
  }

  onSubmit(form:NgForm){
    debugger;
    if(this.service.FormData.id>0)
    {
      this.updateRecord(form);
    }
 
  }
  updateRecord(form:NgForm){
    this.service.EditUser(this.fileToUpload).subscribe(
      res=>{
        this.toastr.success('Updated Successfully','Register Detail');
        this.resetForm(form);
        this.router.navigate(['/pagination']);
      },
      err => {
           
        if( err.status == 500 ) {
  
          this.toastr.error('Internal Server error');
        } else if( err.status == 400 ) {
    
          this.toastr.error('Unauthorize User');
        }
      }     
    )
  }
}
