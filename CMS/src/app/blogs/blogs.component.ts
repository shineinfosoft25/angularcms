import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BlogService } from '../blog/blog.service';
import { Registration } from '../registration/signin.model';


declare var $:any;
@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {
  Blog = [];
  User : any;  
  pageNo: any = 1;  
  pageNumber: boolean[] = [];  
  sortOrder: any = 'Blogs';  
  pageField = [];  
  exactPageList: any;  
  paginationData: number;  
  UserPerPage: any = 5;  
  totalUsers: any;  
  totalUsersCount: any;
  constructor(public service: BlogService, public toastr:ToastrService,public router:Router,public route:ActivatedRoute) {

    $(document).ready(function(){
      $("#container1").show();    
    });
   }

  ngOnInit(): void {
    this.getAllBlog();
  }

  getAllBlog() {
    debugger;  
      this.service.getAllBlog(this.pageNo, this.UserPerPage, this.sortOrder).subscribe((data: any) => {  
        debugger;
        this.Blog  = data.data.blogs;
        for (let index = 0; index < this.Blog.length; index++) {
          this.Blog [index].image= 'http://192.168.1.68/ImagesPath/'+ this.Blog[index].image;
        }
        this. totalUsersCount = data.data.count;  
        this.totalNoOfPages();       
      })       
    } 
  
  
    onDelete(id)
    {
      if(confirm('Are you sure you want to delete this record?')){
        debugger;
        this.service.DeleteBlog(id)
        .subscribe(
       (res:any) => {
        this.toastr.error('Deleted Successfully','Register Detail');
        this.getAllBlog();
  
  
       },
          err => {
             
            if( err.status == 500 ) {
  
              this.toastr.error('Internal Server error');
            } else if( err.status == 400 ) {
        
              this.toastr.error('Unauthorize User');
            }
          });
  
      }  
    }  
    onEdit(item:Registration){      
      this.router.navigate(['/blog',item.id]);   
    }
    
    totalNoOfPages() {  
      this.paginationData = Number(this. totalUsersCount / this.UserPerPage);  
      let tempPageData = this.paginationData.toFixed();  
      if (Number(tempPageData) < this.paginationData) {  
        this.exactPageList = Number(tempPageData) + 1;  
        this.service.exactPageList = this.exactPageList;  
      } else {  
        this.exactPageList = Number(tempPageData);  
        this.service.exactPageList = this.exactPageList  
      }  
      this.service.pageOnLoad();  
      this.pageField = this.service.pageField;  
    }  
    showCompaniesByPageNumber(page, i) {  
      this.Blog  = [];  
      this.pageNumber = [];  
      this.pageNumber[i] = true;  
      this.pageNo = page;  
      this.getAllBlog();  
    }   





}
