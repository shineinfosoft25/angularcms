import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from './login.service';



declare var $:any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public service:LoginService,public router:Router,
    public toastr:ToastrService) { 

      $(document).ready(function(){
        $("#container1").hide();    
      });
     }  

  ngOnInit(): void {
  }

  onSubmit(email,password){
        this.service.Login(email,password).subscribe(
      (res:any)=>{
        debugger;
        sessionStorage.setItem('token',(res.data.accessToken));       
        this.toastr.success('Login Successfully');
        this.router.navigate(['/dashboard']);
      },
      err=>{     
        this.toastr.error('Invalid Login Details');     
      }
    )
  }




  
}
