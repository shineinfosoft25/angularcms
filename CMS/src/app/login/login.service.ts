import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  // readonly rootURL ='http://localhost:62921/api';
  // http://192.168.1.68/api/users
  readonly rootURL ='http://192.168.1.68/api';
  constructor(public http:HttpClient) { }

Login(email,password){ 
    debugger;  
    var data = "email="+email+"&password="+password+"";
    var reqheader = new HttpHeaders({'Content-type':'application/x-www-urlencoded','No-Auth':'True'});
  
    return this.http.post(this.rootURL+'/email-login?'+data,{headers:reqheader});
  }
  getToken(){
    return localStorage.getItem('token');
  }

}
