import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TestimonialService } from '../testimonial/testimonial.service';


declare var $:any;
@Component({
  selector: 'app-edittestimonial',
  templateUrl: './edittestimonial.component.html',
  styleUrls: ['./edittestimonial.component.css']
})
export class EdittestimonialComponent implements OnInit {
  imageUrl: string = "/assets/Image/user.jpg";
  readonly rootURL ='http://192.168.1.68/api';
  fileToUpload: File = null;
  constructor(public service:TestimonialService, public toastr:ToastrService,public router:Router,public route:ActivatedRoute,public http: HttpClient) { 

    $(document).ready(function(){
      $("#container1").show();    
    });
  }

  ngOnInit(): void {

    this.resetForm(); 
    this.route.paramMap.subscribe(res=>{ 
      const id = +res.get('id');     
      this.GetTestimonialByID(id);
    });
  }

  GetTestimonialByID(id) {
    debugger;
    this.service.GetTestimonialByID(id).subscribe((data: any) => {
      debugger;
        this.imageUrl=  "http://192.168.1.68/ImagesPath/"+data.data.image;
        this.service.FormData={
        id:data.data.id,
        Title:data.data.title,
        Name:data.data.name,
        Description:data.data.description,
        Designation:data.data.designation,
        createdBy:data.data.createdBy,        
        Image:data.data.image,
        Status:1            
      };   
    },
    )

  }


  
  handleFileInput(file:FileList)
  {
    this.fileToUpload = file.item(0);
    var reader = new FileReader();
    reader.onload =(event:any)=>{
    this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }

  resetForm(form?:NgForm){
    if(form!=null)
    form.resetForm();
    this.service.FormData={
      id:null,
      Name:'',
      Title:'',
      Description:'',
      Designation:'',  
      createdBy:'',   
      Image:'',
      Status:null
    }
  }

  onSubmit(form:NgForm){
    debugger;
    if(this.service.FormData.id>0)
    {
      this.updateRecord(form);
    }
 
  }
  updateRecord(form:NgForm){
    this.service.EditTestimonial(this.fileToUpload).subscribe(
      res=>{
        this.toastr.success('Updated Successfully','Testimonial Details');
        this.resetForm(form);
        this.router.navigate(['/testimoniallist']);
      },
      err => {
           
        if( err.status == 500 ) {
  
          this.toastr.error('Internal Server error');
        } else if( err.status == 400 ) {
    
          this.toastr.error('Unauthorize User');
        }
      }     
    )
 
    }


}
