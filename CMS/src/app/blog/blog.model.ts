export class BLOG {
    Id:number;
    BlogTitle:string;
    Description:string;
    MetaTitle:string;
    MetaDescription:string;
    AuthorName:string;
    Image:string="/assests/Image";
    Status:number;
 }
 export class Datum {
    ID: number;
    BlogTitle: string;
    Description:string;
    MetaTitle: string;
    MetaDescription: string;
    AuthorName: number;
    Status: number;
    Image: string;
}
export interface RootObject {
    httpStatusCode: number;
    errorCode: number;
    message: string;
    data: Datum[];
}
