import { Injectable, Injector } from '@angular/core';  
import { HttpClient, HttpHeaders } from '@angular/common/http';  
import { Observable } from 'rxjs';  
import { ToastrService } from 'ngx-toastr';
import { BLOG } from './blog.model';

@Injectable({  
  providedIn: 'root'  
})  
export class BlogService {
  FormData:BLOG;
  // readonly rootURL ='http://localhost:62921/api';


 readonly rootURL ='http://192.168.1.68/api';
  temppage: number = 0;  
  pageField = [];  
  exactPageList: any;  
    private url = "";  
    constructor(public http: HttpClient,private injector:Injector,public toastr:ToastrService) {  
  }
    
    AddBlog(fileToUpload :File)
    {
      debugger;
      const formData:FormData = new FormData();
      formData.append('BlogTitle',this.FormData.BlogTitle); 
      formData.append('Description',this.FormData.Description);
      formData.append('MetaTitle',this.FormData.MetaTitle);
      formData.append('MetaDescription',this.FormData.MetaDescription);
      formData.append('AuthorName',this.FormData.AuthorName);
      formData.append('Image', fileToUpload, fileToUpload.name);
      // formData.append('Status',this.FormData.Status);
      return this.http.post(this.rootURL+'/blogs',formData)
    }
    
    getAllBlog(pageNo,pageSize,sortOrder): Observable<any> {
      debugger;
      this.url = 'http://192.168.1.68/api/blogs?take='+pageSize+'&skip='+(pageNo-1)*pageSize;    
        return this.http.get(this.url);  
      }     

      GetBlogByID(id){
     
        return this.http.get(this.rootURL+'/blogs/'+ id)
      } 
      
      DeleteBlog(id){  
        return this.http.delete(this.rootURL+'/blogs/'+ id)
    
      }   

      EditBlog(fileToUpload: File,){
        debugger;   
        const formData: FormData = new FormData();
        formData.append('BlogTitle', this.FormData.BlogTitle);
        formData.append('Description', this.FormData.Description);
        formData.append('MetaTitle', this.FormData.MetaTitle);
        formData.append('MetaDescription', this.FormData.MetaDescription);
        formData.append('AuthorName', this.FormData.AuthorName);
        formData.append('Image', fileToUpload, fileToUpload.name)
        return this.http.patch(this.rootURL+'/blogs/'+this.FormData.Id,formData);  
      }
      
      pageOnLoad() {  
        debugger;
        if (this.temppage == 0) {  
            this.pageField = [];  
            for (var a = 0; a < this.exactPageList; a++) {  
                this.pageField[a] = this.temppage + 1;  
                this.temppage = this.temppage + 1;  
            }  
        }  
    } 



}  