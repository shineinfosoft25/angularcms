import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(public router:Router,public route:ActivatedRoute) { }

  ngOnInit(): void {

    if(sessionStorage.getItem('token')){  
      this.router.navigate(['/dashboard']);
      }
    
    else
    {
      this.router.navigate(['/']);
    }
  }

}
