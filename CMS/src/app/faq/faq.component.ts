import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FAQService } from './faq.service';



declare var $:any;
@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {

  constructor(public service:FAQService,
    public toastr:ToastrService,public router:Router,public route:ActivatedRoute,public http: HttpClient) { 

      $(document).ready(function(){
        $("#container1").show();    
      });

    }

  ngOnInit(): void {
    this.resetForm();
  }


  resetForm(form?:NgForm){
    if(form!=null)
    form.resetForm();
    this.service.FormData={
      id:null,
      Question:'',
      Answers:'',
      createdBy:'',
  
    }
  }
  
  


onSubmit(form:NgForm){
  debugger;
  if(this.service.FormData.id==null)
  {
    this.insertRecord(form);
  }

}

insertRecord(form:NgForm){
  debugger;
  this.service.AddFAQ().subscribe(
    res=>{
      this.toastr.success('Submitted Successfully','FAQ Detail');
      this.resetForm(form);
      this.router.navigate(['/']);
      },
      err=>{
        console.log(err);       
    }
  )
}




}
