export class FAQ {
    id:number;
    Question:string;
    Answers:string;
    createdBy:string;

 }
 export class Datum {
    id:number;
    Question:string;
    Answers:string;
    createdBy:string;
}
export interface RootObject {
    httpStatusCode: number;
    errorCode: number;
    message: string;
    data: Datum[];
}
