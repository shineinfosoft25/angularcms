import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlogComponent } from './blog/blog.component';
import { BlogsComponent } from './blogs/blogs.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EditblogComponent } from './editblog/editblog.component';
import { EditfaqComponent } from './editfaq/editfaq.component';
import { EdittestimonialComponent } from './edittestimonial/edittestimonial.component';
import { FaqComponent } from './faq/faq.component';
import { FaqsComponent } from './faqs/faqs.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { TestimonialComponent } from './testimonial/testimonial.component';
import { TestimoniallistComponent } from './testimoniallist/testimoniallist.component';
import { UsresComponent } from './usres/usres.component';



const routes: Routes = [
  {path:'registration',component:RegistrationComponent},
  {path:'blogs',component:BlogsComponent},
  {path:'blog',component:BlogComponent},
  {path:'faqs',component:FaqsComponent},
  {path:'faq',component:FaqComponent},
  {path:'testimonial/:id',component:EdittestimonialComponent}, 
  {path:'blog/:id',component:EditblogComponent},
  {path:'faq/:id',component:EditfaqComponent},
  {path:'testimonial',component:TestimonialComponent}, 
  {path:'testimoniallist',component:TestimoniallistComponent},
  {path:'dashboard',component:DashboardComponent },
  {path:'users',component:UsresComponent},
  {path:'',component:LoginComponent,pathMatch:"full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }