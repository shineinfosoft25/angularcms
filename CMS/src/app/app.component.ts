import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'CMS';


  public model = {
    name: 'Ujjaval',
    description: '<p>This is a sample form using CKEditor 4.</p>'
  };

onSubmit() {
    console.log( `Form submit, model: ${ JSON.stringify( this.model ) }` );
}

}
