export class Registration {
   id:number;
    UserName:string;
    Email:string;
    Password:string;
    UserType:string;
    AccessToken?: any;
    Profile:string="/assests/Image";
    Status:number;
}
  
export class Datum {
    ID: number;
    UserName: string;
    Email: string;
    Password: string;
    Status: number;
    AccessToken?: any;
    UserType:string;
    Profile:string;
}
export interface RootObject {
    httpStatusCode: number;
    errorCode: number;
    message: string;
    data: Datum[];
}






