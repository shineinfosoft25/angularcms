import { Injectable, Injector } from '@angular/core';  
import { HttpClient, HttpHeaders } from '@angular/common/http';  
import { Observable } from 'rxjs';  
import { ToastrService } from 'ngx-toastr';
import { Registration } from './signin.model';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable({  
  providedIn: 'root'  
})  
export class ApiService {
  FormData:Registration;
  readonly rootURL ='http://192.168.1.68/api';
  temppage: number = 0;  
  pageField = [];  
  exactPageList: any;  
    private url = "";  
    constructor(public http: HttpClient,private injector:Injector,public toastr:ToastrService,public router:Router,public route:ActivatedRoute) {  
  }
    
  DeleteUser(id){
    const httpOptions = {
    headers: new HttpHeaders({  
    'Authorization': `Bearer ${sessionStorage.getItem('token')}`
    })
  };
  return this.http.delete(this.rootURL+'/users/'+ id,httpOptions)
}
  // DeleteUser(id){  
  //   // const httpOptions = {
  //   //   headers: new HttpHeaders({  
  //   //   'Authorization': `Bearer ${sessionStorage.getItem('token')}`
  //   //   })
  //   // };

  //   if(sessionStorage.getItem('token')){  
  //     return this.http.delete(this.rootURL+'/users/'+ id)
  //     }
    
  //   else
  //   {
  //     this.router.navigate(['/']);
  //   }
 
  // } 
 
  ActiveUser(id){  
    const formData: FormData = new FormData();
    return this.http.patch(this.rootURL+'/users/'+id,formData)
  } 


  GetUserByID(id){
    debugger;
        const httpOptions = {
          headers: new HttpHeaders({      
          'Authorization': `Bearer ${sessionStorage.getItem('token')}`
          })
        };     
        return this.http.get(this.rootURL+'/users/'+ id,httpOptions)
      }   
      
  EditUser(fileToUpload: File,){
    debugger;
    const httpOptions = {
      headers: new HttpHeaders({      
      'Authorization': `Bearer ${localStorage.getItem('token')}`
      })
    };
    const formData: FormData = new FormData();
    formData.append('UserName', this.FormData.UserName);
    formData.append('UserType', this.FormData.UserType);
    formData.append('Email', this.FormData.Email);
    formData.append('Password', this.FormData.Password);
    formData.append('Profile', fileToUpload, fileToUpload.name)
    return this.http.patch(this.rootURL+'/users/'+this.FormData.id,formData,httpOptions);
  }
    
  AddUser(fileToUpload: File){
    debugger;
    const formData: FormData = new FormData();
    formData.append('UserName', this.FormData.UserName);
    formData.append('Email', this.FormData.Email);
    formData.append('Password', this.FormData.Password);
    formData.append('UserType', this.FormData.UserType);   
    formData.append('Profile', fileToUpload, fileToUpload.name);
    return this.http.post(this.rootURL+'/users',formData);
  }

  getAllUser(pageNo,pageSize,sortOrder): Observable<any> {
    debugger;
    this.url = 'http://192.168.1.68/api/users?take='+pageSize+'&skip='+(pageNo-1)*pageSize;    
      return this.http.get(this.url);  
    }    

  pageOnLoad() {  
    debugger;
    if (this.temppage == 0) {  
        this.pageField = [];  
        for (var a = 0; a < this.exactPageList; a++) {  
            this.pageField[a] = this.temppage + 1;  
            this.temppage = this.temppage + 1;  
        }  
    }  
}
}  