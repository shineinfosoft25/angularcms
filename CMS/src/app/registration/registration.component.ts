import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from './api.service';

declare var $:any;
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  imageUrl: string = "/assets/user.jpg";
  fileToUpload: File = null;
  constructor(public service:ApiService,
    public toastr:ToastrService,public router:Router,public route:ActivatedRoute,public http: HttpClient) { 

      $(document).ready(function(){
        $("#container1").hide();    
      });
    }

  ngOnInit(): void {
    this.resetForm();

  }
  
  handleFileInput(file:FileList)
  {
    this.fileToUpload = file.item(0);
    var reader = new FileReader();
    reader.onload =(event:any)=>{
    this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  } 

  resetForm(form?:NgForm){
    if(form!=null)
    form.resetForm();
    this.service.FormData={
      id:null,
      UserName:'',
      Email:'',
      Password:'',
      UserType:'',
      Profile:'',
      Status:null
    }
  }

  onSubmit(form:NgForm){
    debugger;
    if(this.service.FormData.id==null)
    {
      this.insertRecord(form);
    }
  }

  insertRecord(form:NgForm){
    debugger;
    this.service.AddUser(this.fileToUpload).subscribe(
      res=>{
        this.toastr.success('Submitted Successfully','Register Detail');
        this.resetForm(form);
        this.router.navigate(['/login']);
        },
        err=>{
          console.log(err);       
      }
    )
  }
}
