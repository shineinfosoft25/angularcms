import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BlogService } from '../blog/blog.service';


declare var $:any;
@Component({
  selector: 'app-editblog',
  templateUrl: './editblog.component.html',
  styleUrls: ['./editblog.component.css']
})
export class EditblogComponent implements OnInit {
  imageUrl: string = "/assets/Image/user.jpg";
  readonly rootURL ='http://192.168.1.68/api';
  fileToUpload: File = null;

  constructor(public service:BlogService, public toastr:ToastrService,public router:Router,public route:ActivatedRoute,public http: HttpClient) {
     $(document).ready(function(){
      $("#container1").show();    
    });
   }

  ngOnInit(): void {
    this.resetForm(); 
    this.route.paramMap.subscribe(res=>{ 
      const id = +res.get('id');     
      this.GetUserByID(id);
    });
  }

  GetUserByID(id) {
    debugger;
    this.service.GetBlogByID(id).subscribe((data: any) => {
      debugger;
        this.imageUrl=  "http://192.168.1.68/ImagesPath/"+data.data.image;
        this.service.FormData={
        Id:data.data.id,
        BlogTitle:data.data.blogTitle,
        Description:data.data.description,
        MetaTitle:data.data.metaTitle,
        MetaDescription:data.data.metaDescription,
        AuthorName:data.data.authorName,
        Image:data.data.image,
        Status:1            
      };   
    },
    )

  }  
  handleFileInput(file:FileList)
  {
    this.fileToUpload = file.item(0);
    var reader = new FileReader();
    reader.onload =(event:any)=>{
    this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  } 
  resetForm(form?:NgForm){
    if(form!=null)
    form.resetForm();
    this.service.FormData={
  Id:null,
  BlogTitle:'',
  Description:'',
  MetaTitle:'',
  MetaDescription:'',
  AuthorName:'',
  Image:null,
  Status:null
    }
  }
  onSubmit(form:NgForm){
    debugger;
    if(this.service.FormData.Id>0)
    {
      this.updateRecord(form);
    } 
  }
  updateRecord(form:NgForm){
    this.service.EditBlog(this.fileToUpload).subscribe(
      res=>{
        this.toastr.success('Updated Successfully','Register Detail');
        this.resetForm(form);
        this.router.navigate(['/blogs']);
      },
      err => {
           
        if( err.status == 500 ) {
  
          this.toastr.error('Internal Server error');
        } else if( err.status == 400 ) {
    
          this.toastr.error('Unauthorize User');
        }
      }     
    ) 
}
}
