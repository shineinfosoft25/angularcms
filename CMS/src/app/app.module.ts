import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BlogComponent } from './blog/blog.component';
import { ApiService } from './registration/api.service';
import { BlogService } from './blog/blog.service';
import { EditblogComponent } from './editblog/editblog.component';
import { FaqComponent } from './faq/faq.component';
import { EditfaqComponent } from './editfaq/editfaq.component';
import { CKEditorModule } from 'ckeditor4-angular';
import { TestimonialComponent } from './testimonial/testimonial.component';
import { TestimoniallistComponent } from './testimoniallist/testimoniallist.component';
import { EdittestimonialComponent } from './edittestimonial/edittestimonial.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BlogsComponent } from './blogs/blogs.component';
import { FaqsComponent } from './faqs/faqs.component';
import { UsresComponent } from './usres/usres.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    BlogComponent,
    BlogsComponent,
    EditblogComponent,
    FaqsComponent,
    FaqComponent,
    EditfaqComponent,
    TestimonialComponent,
    TestimoniallistComponent,
    EdittestimonialComponent,
    DashboardComponent,
    UsresComponent,

  ],
  imports: [
    BrowserModule,  
    HttpClientModule,
    BrowserAnimationsModule, 
    AppRoutingModule,  
    ToastrModule.forRoot(),  
    FormsModule,
    ReactiveFormsModule,
    CKEditorModule,
    
  ],
 
  providers: [ApiService,BlogService],
  bootstrap: [AppComponent],
 
})
export class AppModule { }
