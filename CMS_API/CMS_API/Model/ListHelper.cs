﻿namespace CMS_API.Model
{
    public class ListHelper
    {
        public string query { get; set; } = string.Empty;
        public string sortBy { get; set; } = string.Empty;
        public int status { get; set; }
        public int take { get; set; } 
        public int skip { get; set; } = 0;
        public string  Filter { get; set; }= string.Empty;
    }
}
