﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMS_API.Model
{

    public class Users
    {
        public List<User> users { get; set; }
        public long count { get; set; }
    }

    public class User
    {
        public int ID { get; set; }

        public string UserName { get; set; }
        public string Email { get; set; }

        public string Password { get; set; }

        public int Status { get; set; }
        public string AccessToken { get; set;}
        public string UserType { get; set; }
        public string Profile { get; set; }

        //public string CreatedBy { get; set; }
        public string  CreatedBy { get; set; }
        public int TotalCount { get; set; }

    }
}

