﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMS_API.Model
{
    public class FAQS
    {
        public List<FAQ> faqs { get; set; }

        public long count { get; set; }

    }
    public class BaseResponse
    {
        public string createdBy { get; set; }
    }
    public class FAQ:BaseResponse
    {
        public int ID { get; set; }
        public string Question { get; set; }
        public string Answers { get; set; } 

        public int TotalCount { get; set; }


    }
}


