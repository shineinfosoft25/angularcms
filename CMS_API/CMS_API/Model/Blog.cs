﻿using System;
using System.Collections.Generic;


namespace CMS_API.Model
{
        public class Blogs
    {
        public List<Blog> blogs { get; set; }

        public long count { get; set; }

    }
        public class Blog
    {
        public int Id { get; set; }

        public string BlogTitle { get; set; }
        
        public string Description { get; set; }

        public string Image { get; set; }

        public string MetaTitle { get; set; }

        public string MetaDescription { get; set; }

        public string AuthorName { get; set; }

        public DateTime PublishDate { get; set; }

        public int status { get; set;}


        public int TotalCount { get; set; }

    }
}
