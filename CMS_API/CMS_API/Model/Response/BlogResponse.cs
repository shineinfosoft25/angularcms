﻿using System.Collections.Generic;

namespace CMS_API.Model.Response
{
    public class BlogResponse
    {
        public int total { get; set; }

        public List<Blog> blogs { get; set; }
    }
}
