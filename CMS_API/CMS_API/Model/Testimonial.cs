﻿using System.Collections.Generic;


namespace CMS_API.Model
{
    public class Testimonials
    {
        public List<Testimonial> testimonials { get; set; }

        public long count { get; set; }

    }    
    public class Testimonial
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string createdBy { get; set; }
        public string Image { get; set; }
        public int  Status { get; set; }
        public string Designation { get; set; }
    }
}


