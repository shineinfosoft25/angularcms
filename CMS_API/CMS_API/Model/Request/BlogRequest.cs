﻿namespace CMS_API.Model.Request
{
    public class BlogRequest
    {

        public int PageIndex { get; set; } = 1;

        public int PageSize { get; set; } = 30;

        public string SearchKeyword { get; set; }
    }
}
