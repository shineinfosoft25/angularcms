using System.ComponentModel.DataAnnotations;

namespace CMS_API.Model
{
  public class EmailLoginRequest
  {
    [Required]
    public string Email { get; set; }

    [Required]
    public string Password { get; set; }
  }
}
