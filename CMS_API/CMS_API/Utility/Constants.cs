namespace CMS_API.Utility
{
    public class Constants
    {
        
        #region StoredProcedure
        #region UserSps
        public const string CheckEmailSP = "CheckEmail_sp";
        public const string AddUserSP = "Adduser_sp";
        public const string GetUserByIDSP = "GetUserByID_SP";
        public const string EditUsersp = "EditUser_sp";
        public const string AddBlogSp = "Addblog_SP";
        public const string GetAllBlogsSp = "GetAllBlogs_Sp";
        public const string GetAllBlogCountSp = "GetAllBlogCount_Sp";
        public const string GetBlogByIDSP = "GetBlogByID_SP";
        public const string EDITBLOGSP = "Editblog_sp";
        public const string AddFAQsp = "AddFAQ_sp";
        public const string GetALlFAQSp = "GetALlFAQ_Sp";
        public const string GetAllFAQCount_Sp= "GetAllFAQCount_Sp";
        public const string GetFAQByIDsp = "GetFAQByID_sp";
        public const string EditFaqSP = "EditFaq_SP";
        public const string DeleteFAQsp = "DeleteFAQ_sp";
        public const string AddTestimonialsp = "AddTestimonial_sp";
        public const string GetALLTestimonialSp = "GetALLTestimonial_Sp";
        public const string GetAllTestimonialCountSp= "GetAllTestimonialCount_Sp";
        public const string DeleteTestimonialsp= "DeleteTestimonial_sp";
        public const string GetAllTestimonialBYIDsp= "GetAllTestimonialBYID_sp";
        public const string EditTestimonialSP= "EditTestimonial_SP";
        public const string FindAccountsp= "FindAccount_sp";
        public const string GetAllUserssp = "GetAllUsers_sp";
        public const string GetAllUsersCountSp = "GetAllUsersCount_Sp";
        public const string DeleteUsersp = "DeleteUser_sp";
        public const string ActiveUsersp = "ActiveUser_sp";

        #endregion
        #endregion
        #region ErrorMessage

        public const string UserName = "User Name can not be null";
        public const string UserType = "User Type can not be null";
        public const string Question = "Question can not be null";
        public const string Answer = "Answer can noe be null";
        public const string CreatedBy = "Created by can  not be null";
        //public const string Title = "Title not ne null";

        public const string UserNotFound = "User not found";
        public const string UserRegistered = "User already registered";
        public const string Email = "Email can not be null";
        public const string Password = "Password can not be null";
        public const string Error = "Oops something went wrong";
        public const string EmailError = "This email address already registered with us";
        public const string InvalidLoginDetails = "Invalid login details.Please try again";
        public const string ValidLoginDetails = "You have successfully logged in";
        public const string InvalidRequest = "invalid request";
        public const string ProfileUpateError = "Error occured while updating user profile image";
        public const string InvalidUserID = "Invalid user id";
        public const string UserDeletedString = "Your account has been deactivated.";
  

    #endregion ErrorMessage
        #region Success

        public const string Saved = "Saved Successfully.";
        public const string Success = "Successfully.";
        public const string Update = "Update Successfully.";
        public const string Image = "Profile image updated Successfully.";
        public const string Delete = "Delete Successfully.";
        public const string ProfileUpate = "Profile image updated successfully";

        #endregion Success
        #region ApiInfo
        public const string APIVersion = "1.1";
        public const string APIVersionWithPrefix = "v1.1";
        public const string Title = "CMS";
        public const string Description = "CMS";
        public const string APIUrl = "http://api.streetarttagger.com/";
        #endregion

    }
}
