using System;
using System.Net;

namespace CMS_API.Utility
{
	public class JsonResponse
	{
		public JsonResponse()
		{
			HttpStatusCode = 0;
			Message = "";
			Data = new object();
		}

		public int HttpStatusCode { get; set; }

		public int ErrorCode { get; set; }

		public string Message { get; set; }

		public object Data { get; set; }
	}
	public static partial class Helper
	{
		#region jsonconversion

		public static JsonResponse UserBlocked(string message, object data = null, int errorCode = 1)
		{
			JsonResponse response = new JsonResponse();
			response.HttpStatusCode = 409;
			response.ErrorCode = errorCode;
			response.Message = message ?? string.Empty;
			response.Data = data;
			return response;
		}

		public static JsonResponse GenerateBadRequest(string message, object data = null, int errorcode = 1)
		{
			JsonResponse response = new JsonResponse();
			response.HttpStatusCode = (int)HttpStatusCode.BadRequest;
			response.ErrorCode = errorcode;
			response.Message = message ?? string.Empty;
			response.Data = data ?? "";
			return response;
		}

		public static JsonResponse GenerateSuccess(string message, object data = null)
		{
			JsonResponse response = new JsonResponse();
			response.HttpStatusCode = (int)HttpStatusCode.OK;
			response.ErrorCode = 0;
			response.Message = message ?? string.Empty;
			response.Data = data;
			return response;
		}

     

        #endregion jsonconversion
    }
}
