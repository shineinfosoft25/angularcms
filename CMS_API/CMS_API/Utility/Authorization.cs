﻿using CMS_API.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;


namespace CMS_API.Utility
{
    public class Authorization
    {
        #region Methods
        public string GenerateJwtToken(IConfiguration Configuration, User user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[] {
                    new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Email, user.Email),
                    new Claim(JwtRegisteredClaimNames.NameId, user.ID.ToString()),
                    new Claim("Status", user.Status.ToString()),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };
            var token = new JwtSecurityToken(Configuration["Jwt:Issuer"],
              Configuration["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddDays(120),
              signingCredentials: credentials);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        //public string GenerateJwtToken(IConfiguration configuration,User user)
        //{
        //    var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"]));
        //    var Credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

        //    var claims = new[]
        //    {
        //    new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
        //    new Claim(JwtRegisteredClaimNames.Email,user.Email),
        //    new Claim(JwtRegisteredClaimNames.NameId, user.ID.ToString()),

        //    new Claim("Status",user.Status.ToString()),
        //    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())
        //    };

        //    var token = new JwtSecurityToken(configuration["Jwt:Issuer"],
        //      configuration["Jwt:Issuer"],
        //      claims,
        //      expires: DateTime.Now.AddDays(120),
        //      signingCredentials: Credentials);
        //    return new JwtSecurityTokenHandler().WriteToken(token);
        //}

        //public static int GetUserId(HttpContext httpcontext)
        //{
        //    if (httpcontext.User != null && httpcontext.User.Claims != null && httpcontext.User.Claims.Count() > 0)
        //    {
        //        return Convert.ToInt32(httpcontext.User.Claims.First(i => i.Type == ClaimTypes.NameIdentifier).Value);
        //    }
        //    else
        //    {
        //        return 0;
        //    }

        //}

        public static int GetUserId(HttpContext httpContext)
        {
            if (httpContext.User != null && httpContext.User.Claims != null && httpContext.User.Claims.Count() > 0)
            {
                return Convert.ToInt32(httpContext.User.Claims
                             .First(i => i.Type == ClaimTypes.NameIdentifier).Value);
            }
            else
                return 0;
        }
    }


    #endregion
}
