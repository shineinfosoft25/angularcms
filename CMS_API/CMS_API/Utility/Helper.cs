using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using HttpContext = Microsoft.AspNetCore.Http.HttpContext;


namespace CMS_API.Utility
{
    public partial class Helper
    {

        #region Properties
        public static SqlConnection con { get; set; }
        public static IConfiguration Configuration { get; set; }


        #endregion

        #region easymethod
        public static string getFormDataItem(HttpContext httpContext, string key)
        {
            return httpContext.Request.Form[key].ToString() ?? string.Empty;
        }
        #endregion

        #region Connection
        public static string GetConnectionString()
    {
      return Configuration.GetConnectionString("GetConnection");
    }

    public static SqlConnection GetConnection()
    {
      if (con == null)
      {
        con = new SqlConnection(GetConnectionString());
        ConnectionCheck();
      }
      return con;
    }

    public static void ConnectionCheck()
    {
      if (con != null)
      {
        if (con.State == ConnectionState.Closed)
        {
          con.Open();
        }
      }
    }
    #endregion

    public static string HashPassword(string password)
        {
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
            byte[] hash = pbkdf2.GetBytes(20);
            byte[] hashbytes = new byte[36];
            Array.Copy(salt, 0, hashbytes, 0, 16);
            Array.Copy(hash, 0, hashbytes, 16, 20);
            return Convert.ToBase64String(hashbytes);

        }
        public static bool VerifyPassword(string password, string hashedPassword)
        {
          byte[] hashBytes = Convert.FromBase64String(hashedPassword);
          byte[] salt = new byte[16];
          Array.Copy(hashBytes, 0, salt, 0, 16);
          var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
          byte[] hash = pbkdf2.GetBytes(20);
          for (int i = 0; i < 20; i++)
          {
            if (hashBytes[i + 16] != hash[i])
              return false;
          }
          return true;
        }
  }
}
