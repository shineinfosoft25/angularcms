﻿using CMS_API.Model;
using CMS_API.Utility;
using Dapper;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


namespace CMS_API.Repository
{
    public class BlogRepository
    {
        #region Methods
        public bool AddBlog(Blog blog)
        {
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@BlogTitle", blog.BlogTitle);
                param.Add("@Description", blog.Description);
                param.Add("@Image", blog.Image);
                param.Add("@MetaTitle", blog.MetaTitle);
                param.Add("@MetaDescription", blog.MetaDescription);
                param.Add("@AuthorName", blog.AuthorName);
                param.Add("@Status", 1);

                db.Execute(Constants.AddBlogSp, param, commandType: CommandType.StoredProcedure);
                return true;
            }
        }
        public Blogs GetAllBlogs(ListHelper listHelper)
        {
            Blogs blogs = new Blogs();
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@query", listHelper.query);
                param.Add("@sortBy", listHelper.sortBy);
                param.Add("@take", listHelper.take);
                param.Add("@skip", listHelper.skip);
                blogs.blogs = db.Query<Blog>(Constants.GetAllBlogsSp, param, commandType: CommandType.StoredProcedure)?.ToList();
                blogs.count = Convert.ToInt64(db.ExecuteScalar(Constants.GetAllBlogCountSp, commandType: CommandType.StoredProcedure)?.ToString());
                return blogs;
            }
        }
        public Blog GetBlogByID(int id)
        {
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@Id", id);
                var DBResutl = db.Query<Blog>(Constants.GetBlogByIDSP, param, commandType: CommandType.StoredProcedure)?.ToList();
                if (DBResutl.Count > 0)
                {
                    return DBResutl.First();
                }
                else
                {
                    return null;
                }
            }
        }
        public bool EditBlog(Blog blog)
        {
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("Id", blog.Id);
                param.Add("@BlogTitle", blog.BlogTitle);
                param.Add("@Description", blog.Description);
                param.Add("@MetaTitle", blog.MetaTitle);
                param.Add("@MetaDescription", blog.MetaDescription);
                param.Add("@Image", blog.Image);
                param.Add("@AuthorName", blog.AuthorName);
                param.Add("@STATUS", blog.status);

                db.Execute(Constants.EDITBLOGSP, param, commandType: CommandType.StoredProcedure);
                return true;
            }
        }
        #endregion
    }
}





