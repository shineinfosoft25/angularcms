﻿using CMS_API.Model;
using CMS_API.Utility;
using Dapper;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CMS_API.Repository
{
    public class UserRepository
    {
        #region Methods

        public User AddUser(User user, int Status = 1)
        {
            user.Status = 1;
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@UserName", user.UserName.Trim());
                param.Add("@Email", user.Email.Trim());
                param.Add("@Password", user.Password.Trim());
                param.Add("@Status", Status);
                param.Add("@UserType", user.UserType.Trim());
                param.Add("@Profile", user.Profile.Trim());
                param.Add("@CreatedBy", user.CreatedBy);

                var DBResutl = db.Query<User>(Constants.AddUserSP, param, commandType: CommandType.StoredProcedure)?.ToList();
                if (DBResutl.Count > 0)
                {
                    return DBResutl.First();
                }
                else
                {
                    return null;
                }
            }
        }

        //public Users GetAllUsers(ListHelper listHelper)
        //{
        //    Users users = new Users();
        //    using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
        //    {
        //        DynamicParameters param = new DynamicParameters();
        //        param.Add("@query", listHelper.query);
        //        param.Add("@sortBy", listHelper.sortBy);
        //        param.Add("@take", listHelper.take);
        //        param.Add("@skip", listHelper.skip);

        //        users.users = db.Query<User>(Constants.GetAllUserssp, param, commandType: CommandType.StoredProcedure)?.ToList();
        //        users.count = Convert.ToInt64(db.ExecuteScalar(Constants.GetAllUsersCountSp, commandType: CommandType.StoredProcedure)?.ToString());
        //        return users;
        //    }
        //}

        public Users GetAllUsers(ListHelper listHelper)
        {
            Users faqs = new Users();
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@query", listHelper.query);
                param.Add("@sortBy", listHelper.sortBy);
                param.Add("@take", listHelper.take);
                param.Add("@skip", listHelper.skip);
                param.Add("@status", listHelper.status);
                faqs.users = db.Query<User>(Constants.GetAllUserssp, param, commandType: CommandType.StoredProcedure)?.ToList();
                faqs.count = Convert.ToInt64(db.ExecuteScalar(Constants.GetAllUsersCountSp, commandType: CommandType.StoredProcedure)?.ToString());
                return faqs;
            }
        }


        public bool DeleteUser(int id)
        {
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@ID", id);
                db.Execute(Constants.DeleteUsersp, param, commandType: CommandType.StoredProcedure);
                return true;
            }
        }


        public bool ActiveUser(int id)
        {
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@ID", id);
                db.Execute(Constants.ActiveUsersp, param, commandType: CommandType.StoredProcedure);
                return true;
            }
        }
        public User CheckEmail(string Email)
        {
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@Email", Email);
                var DBResult = db.Query<User>(Constants.CheckEmailSP, param, commandType: CommandType.StoredProcedure)?.ToList();
                if (DBResult.Count > 0)
                {
                    return DBResult.First();
                }
                else
                {
                    return null;
                }
            }
        }
        public User GetUserByID(int id)
        {
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@ID", id);
                var DBResutl = db.Query<User>(Constants.GetUserByIDSP, param, commandType: CommandType.StoredProcedure)?.ToList();
                if (DBResutl.Count > 0)
                {
                    return DBResutl.First();
                }
                else
                {
                    return null;
                }
            }
        }
        public bool EditUser(User user)
        {
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("ID", user.ID);
                param.Add("@UserName", user.UserName.Trim());
                param.Add("@Email", user.Email.Trim());
                param.Add("@Password", user.Password.Trim());
                param.Add("@Status", user.Status);
                param.Add("@Profile", user.Profile.Trim());
                db.Execute(Constants.EditUsersp, param, commandType: CommandType.StoredProcedure);
                return true;
            }
        }
        public User FindAccount(string Email)
        {
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                //param.Add("@Action", 1);
                param.Add("@Email", Email);
                var DBResult = db.Query<User>(Constants.FindAccountsp, param, commandType: CommandType.StoredProcedure)?.ToList();
                if (DBResult.Count > 0)
                {
                    return DBResult.First();
                }
                else { return null; }
            }
        }

        #endregion
    }
}
