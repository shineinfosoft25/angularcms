﻿using CMS_API.Model;
using CMS_API.Utility;
using Dapper;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


namespace CMS_API.Repository
{
    public class FAQRepository
    {

        #region Methods

        public bool AddFAQ(FAQ faq)
        {
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@Question", faq.Question);
                param.Add("@Answers", faq.Answers);
                param.Add("@createdBy", faq.createdBy);
                param.Add("@status", 1);
                db.Execute(Constants.AddFAQsp, param, commandType: CommandType.StoredProcedure);
                return true;

            }
        }
        public FAQS GetAllFAQS(ListHelper listHelper)
        {
            FAQS faqs = new FAQS();
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@query", listHelper.query);
                param.Add("@sortBy", listHelper.sortBy);
                param.Add("@take", listHelper.take);
                param.Add("@skip", listHelper.skip);
                faqs.faqs = db.Query<FAQ>(Constants.GetALlFAQSp, param, commandType: CommandType.StoredProcedure)?.ToList();
                faqs.count = Convert.ToInt64(db.ExecuteScalar(Constants.GetAllFAQCount_Sp, commandType: CommandType.StoredProcedure)?.ToString());
                return faqs;
            }
        }
        public FAQ GetFAQByID(int id)
        {
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@ID", id);
                var DBResutl = db.Query<FAQ>(Constants.GetFAQByIDsp, param, commandType: CommandType.StoredProcedure)?.ToList();
                if (DBResutl.Count > 0)
                {
                    return DBResutl.First();
                }
                else
                {
                    return null;
                }
            }
        }
        public bool EditFAQ(FAQ faq)
        {
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@ID", faq.ID);
                param.Add("@Question", faq.Question);
                param.Add("@Answers", faq.Answers);
                param.Add("@createdBy", faq.createdBy);

                db.Execute(Constants.EditFaqSP, param, commandType: CommandType.StoredProcedure);
                return true;
            }
        }
        public bool DeleteFAQ(int id)
        {
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@ID", id);
                db.Execute(Constants.DeleteFAQsp, param, commandType: CommandType.StoredProcedure);
                return true;
            }
        }

        #endregion


    }
}
