﻿using CMS_API.Model;
using CMS_API.Utility;
using Dapper;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace CMS_API.Repository
{
    public class TestimonialRepository
    {
        #region Methods



        public Testimonial AddTestimonial(Testimonial testimonial, int Status = 1)
        {
            testimonial.Status = 1;
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@Title", testimonial.Title.Trim());
                param.Add("@Name", testimonial.Name.Trim());
                param.Add("@Description", testimonial.Description.Trim());
                param.Add("@Image", testimonial.Image.Trim());
                param.Add("@createdBy", testimonial.createdBy.Trim());
                param.Add("@Status", Status);
                param.Add("@Designation", testimonial.Designation.Trim());

                var DBResutl = db.Query<Testimonial>(Constants.AddTestimonialsp, param, commandType: CommandType.StoredProcedure)?.ToList();
                if (DBResutl.Count > 0)
                {
                    return DBResutl.First();
                }
                else
                {
                    return null;
                }
            }
        }
        public Testimonials GetAllTestimonial(ListHelper listHelper)
        {
            Testimonials testimonials = new Testimonials();
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@query", listHelper.query);
                param.Add("@sortBy", listHelper.sortBy);
                param.Add("@take", listHelper.take);
                param.Add("@skip", listHelper.skip);
                //param.Add("@Status", listHelper.Filter);
                testimonials.testimonials = db.Query<Testimonial>(Constants.GetALLTestimonialSp, param, commandType: CommandType.StoredProcedure)?.ToList();
                testimonials.count = Convert.ToInt64(db.ExecuteScalar(Constants.GetAllTestimonialCountSp, commandType: CommandType.StoredProcedure)?.ToString());
                return testimonials;
            }
        }
        public Testimonial GetTestimonialByID(int id)
        {
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@ID", id);
                var DBResutl = db.Query<Testimonial>(Constants.GetAllTestimonialBYIDsp, param, commandType: CommandType.StoredProcedure)?.ToList();
                if (DBResutl.Count > 0)
                {
                    return DBResutl.First();
                }
                else
                {
                    return null;
                }
            }
        }
        public bool DeleteTestimonial(int id)
        {
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@ID", id);
                db.Execute(Constants.DeleteTestimonialsp, param, commandType: CommandType.StoredProcedure);
                return true;
            }
        }
        public bool EditTestimonial(Testimonial testimonial)
        {
            using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@ID", testimonial.ID);
                param.Add("@Title", testimonial.Title);
                param.Add("@Name", testimonial.Name);
                param.Add("@Description", testimonial.Description);
                param.Add("@Designation", testimonial.Designation);
                param.Add("@createdBy", testimonial.createdBy);
                param.Add("@Image", testimonial.Image);

                db.Execute(Constants.EditTestimonialSP, param, commandType: CommandType.StoredProcedure);
                return true;
            }
        }
        #endregion
    }
}

