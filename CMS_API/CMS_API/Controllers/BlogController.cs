﻿using System;
using System.Collections.Generic;
using System.IO;
using CMS_API.Model;
using CMS_API.Repository;
using CMS_API.Utility;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace CMS_API.Controllers
{

    public class BlogController : ControllerBase
    {
        #region Properties
        public IConfiguration Configuration { get; }

        private BlogRepository blogrepository = new BlogRepository();

        #endregion Properties
        #region Constructor
        public BlogController(IConfiguration _Configuration)
        {
            Configuration = _Configuration;
        }
        #endregion Constructor
        #region Methods
        [HttpPost]
        [Route("api/blogs")]
        public IActionResult AddBlog()
        {
            Blog blog = new Blog();
            blog.BlogTitle = HttpContext.Request.Form["BlogTitle"].ToString() ?? string.Empty;
            blog.Description = HttpContext.Request.Form["Description"].ToString() ?? string.Empty;
            blog.MetaTitle = HttpContext.Request.Form["MetaTitle"].ToString() ?? string.Empty;
            blog.MetaDescription = HttpContext.Request.Form["MetaDescription"].ToString() ?? string.Empty;
            blog.AuthorName = HttpContext.Request.Form["AuthorName"].ToString() ?? string.Empty;

            if (blog != null)
            {
                if (Request?.Form?.Files?.Count > 0)
                {
                    var file = Request.Form.Files[0];
                    string path = System.IO.Path.Combine(Helper.Configuration["ImagesPath"]);
                    string imageName = blog.BlogTitle.Trim() + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + ".jpg";
                    string imgPath = System.IO.Path.Combine(path, imageName);
                    if (file.ContentType.Contains("jpeg") || file.ContentType.Contains("jpg") || file.ContentType.Contains("png"))
                    {
                        using (var fileStream = new FileStream(imgPath, FileMode.Create))
                        {
                            file.CopyTo(fileStream);
                        }
                    }
                    else
                    {
                        return BadRequest(Helper.GenerateBadRequest(Constants.UserType));
                    }
                    blog.Image = imageName;
                }
                if (string.IsNullOrEmpty(blog.BlogTitle.Trim()))
                {
                    return BadRequest(Helper.GenerateBadRequest(Constants.UserName));
                }
                if (string.IsNullOrEmpty(blog.Description.Trim()))
                {
                    return BadRequest(Helper.GenerateBadRequest(Constants.UserName));
                }
                if (blog.MetaTitle == null || blog.MetaTitle.Trim() == "")
                {
                    return BadRequest(Helper.GenerateBadRequest(Constants.Email));
                }
                if (string.IsNullOrEmpty(blog.MetaDescription.Trim()))
                {
                    return BadRequest(Helper.GenerateBadRequest(Constants.Password));
                }
                if (string.IsNullOrEmpty(blog.AuthorName.Trim()))
                {
                    return BadRequest(Helper.GenerateBadRequest(Constants.Password));
                }
                var result = blogrepository.AddBlog(blog);
                return Ok(Helper.GenerateSuccess("Success", result));
            }
            else
            {
                return BadRequest(Helper.GenerateBadRequest(Constants.Error));
            }
        }

        [Route("api/blogs")]
        [HttpGet]
        public IActionResult GetAllBlogs([FromQuery] string query = null, [FromQuery] string sortBy = null, [FromQuery] string Filter = null, [FromQuery] int take = 5, [FromQuery] int skip = 0)
        
        {
            List<Blog> blog = new List<Blog>();
            ListHelper listHelper = new ListHelper();
            listHelper.query = query ?? string.Empty;
            listHelper.sortBy = sortBy ?? string.Empty;
            listHelper.take = take > 0 ? take : 5;
            listHelper.skip = skip > 0 ? skip : 0;       
            var users = blogrepository.GetAllBlogs(listHelper);
            if (users.blogs.Count > 0)
            {
                return Ok(Helper.GenerateSuccess(Constants.Success, users));
            }
            else
            {
                return BadRequest(Helper.GenerateBadRequest(Constants.UserNotFound));
            }
        }


        [Route("api/blogs/{id}")]
        [HttpGet]
        public IActionResult GetBlogsByID(int id)
        {          
               var blog = blogrepository.GetBlogByID(id);
                return Ok(Helper.GenerateSuccess(Constants.Success, blog));          
          
        }

        [HttpPatch]
        [Route("api/blogs/{id}")]
        public IActionResult EditBlog(int id)
        {

             JsonResponse response = new JsonResponse();
                UserRepository userrepository = new UserRepository();


                if (Request.HasFormContentType)
                {
                    if (Request.Form.Files != null || Request.Form.Files.Count > 0)
                    {
                        Blog blog = null;
                    blog = new Model.Blog();
                    blog.Id = id;

                        var userDetail = blogrepository.GetBlogByID(id);

                        if (userDetail == null)
                        {
                            return BadRequest(Helper.GenerateBadRequest(Constants.UserNotFound));
                        }
                        var file = Request.Form.Files[0];
                        string path = Path.Combine(Helper.Configuration["ImagesPath"]);

                        string imageName = blog.Id.ToString().Trim() + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + ".jpg";
                        string imgPath = Path.Combine(path, imageName);
                        using (var fileStream = new FileStream(imgPath, FileMode.Create))
                        {
                            file.CopyTo(fileStream);
                        }

                        userDetail.Image = imageName;
                        Blog user11 = new Blog();
                        user11.BlogTitle = HttpContext.Request.Form["BlogTitle"].ToString() ?? string.Empty;
                        user11.Description = HttpContext.Request.Form["Description"].ToString() ?? string.Empty;
                        user11.MetaTitle = HttpContext.Request.Form["MetaTitle"].ToString() ?? string.Empty;
                        user11.MetaDescription = HttpContext.Request.Form["MetaTitle"].ToString() ?? string.Empty;
                        user11.AuthorName = HttpContext.Request.Form["AuthorName"].ToString() ?? string.Empty;




                        user11.Image = imageName;
                        user11.Id = id;
                        user11.status = 1;
                        var success = new BlogRepository().EditBlog(user11);                       
                      
                        return Ok(Helper.GenerateSuccess(Constants.Update));

                    }
                    else
                    {
                        return BadRequest();
                    }
                }
                else
                {
                    Stream req = Request.Body;
                    string json = new StreamReader(req).ReadToEnd();
                    //User user = null;
                    Blog users = new Blog();
                users.BlogTitle = HttpContext.Request.Form["BlogTitle"].ToString() ?? string.Empty;
                users.Description = HttpContext.Request.Form["Description"].ToString() ?? string.Empty;
                users.MetaTitle = HttpContext.Request.Form["MetaTitle"].ToString() ?? string.Empty;
                users.MetaDescription = HttpContext.Request.Form["MetaTitle"].ToString() ?? string.Empty;
                users.AuthorName = HttpContext.Request.Form["AuthorName"].ToString() ?? string.Empty;

                users.Id = id;
                    users.status = 1;
                var success = new BlogRepository().EditBlog(users);
                return Ok(Helper.GenerateSuccess(Constants.Update));


                }
            }
      
        }
        #endregion
    }


