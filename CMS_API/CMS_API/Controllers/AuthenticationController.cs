using CMS_API.Model;
using CMS_API.Repository;
using CMS_API.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace CMS_API.Controllers
{
  [ApiController]
  public class AuthenticationController : ControllerBase
  {
       #region Properties

    public IConfiguration Configuration { get; }

    private UserRepository userrepository = new UserRepository();

    #endregion Properties

       #region Constructor
    public AuthenticationController(IConfiguration _Configuration)
    {
      Configuration = _Configuration;
    }
        #endregion Constructor


        [Route("api/email-login")]
        [HttpPost]
        public IActionResult EmailLogin(string Email, string Password)
        {
            EmailLoginRequest emailLoginRequest = new EmailLoginRequest();
            //throw new NoNullAllowedException();
            emailLoginRequest.Email = Email;
            emailLoginRequest.Password = Password;
            if (ModelState.IsValid)
            {
                User user = userrepository.CheckEmail(emailLoginRequest.Email);
                if (user == null)
                {
                    return BadRequest(Helper.GenerateBadRequest(Constants.InvalidLoginDetails));
                }
                if (user.Status == 1)
                {
                    if (Helper.VerifyPassword(emailLoginRequest.Password, user.Password))
                    {
                        user.AccessToken = new Authorization().GenerateJwtToken(Configuration, user);
                        return Ok(Helper.GenerateSuccess(Constants.ValidLoginDetails, user));
                    }
                    else
                    {
                        return BadRequest(Helper.GenerateBadRequest(Constants.InvalidLoginDetails));
                    }
                }
                else
                {
                    return BadRequest(Helper.UserBlocked(Constants.UserDeletedString));
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
        }


    }
}
