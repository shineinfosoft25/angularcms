using System;
using CMS_API.Model;
using CMS_API.Repository;
using CMS_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace CMS_API.Controllers
{
    [ApiController]
    public class UserController : ControllerBase
    {
        #region Properties
        public IConfiguration Configuration { get; }

        private UserRepository userrepository = new UserRepository();

        #endregion Properties
        #region Constructor
        public UserController(IConfiguration _Configuration)
        {
            Configuration = _Configuration;
        }
        #endregion Constructor
        #region Methods
        [Route("api/users")]
        [HttpPost]
        public IActionResult AddUser()
        {
            User user = new User();
            //user.ID = Authorization.GetUserId(HttpContext);
            //user.ID = Convert.ToInt32(HttpContext.Request.Form["ID"]);
            user.UserName = HttpContext.Request.Form["UserName"].ToString() ?? string.Empty;
            user.Email = HttpContext.Request.Form["Email"].ToString() ?? string.Empty;
            user.Password = HttpContext.Request.Form["Password"].ToString() ?? string.Empty;
            user.UserType = HttpContext.Request.Form["UserType"].ToString() ?? string.Empty;
          


            if (user != null)
            {

                string CreatedBY = user.UserName.Trim();
                user.CreatedBy = CreatedBY;
                if (Request?.Form?.Files?.Count > 0)
                {
                    var file = Request.Form.Files[0];
                    string path = System.IO.Path.Combine(Helper.Configuration["UserThumb"]);
                    string imageName = user.UserName.Trim() + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + ".jpg";
                    string imgPath = System.IO.Path.Combine(path, imageName);
                  
                    if (file.ContentType.Contains("jpeg") || file.ContentType.Contains("jpg") || file.ContentType.Contains("png"))
                    {
                        using (var fileStream = new FileStream(imgPath, FileMode.Create))
                        {
                            file.CopyTo(fileStream);
                        }
                    }
                    else
                    {
                        return BadRequest(Helper.GenerateBadRequest(Constants.UserType));
                    }
                  
                    user.Profile = imageName;
                }
                if (string.IsNullOrEmpty(user.UserName.Trim()))
                {
                    return BadRequest(Helper.GenerateBadRequest(Constants.UserName));
                }
                if (string.IsNullOrEmpty(user.UserType.Trim()))
                {
                    return BadRequest(Helper.GenerateBadRequest(Constants.UserName));
                }
                if (user.Email == null || user.Email.Trim() == "")
                {
                    return BadRequest(Helper.GenerateBadRequest(Constants.Email));
                }
                if (string.IsNullOrEmpty(user.Password.Trim()))
                {
                    return BadRequest(Helper.GenerateBadRequest(Constants.Password));
                }
                user.Password = Helper.HashPassword(user.Password);
                User users = userrepository.AddUser(user);
                if (user.Status == 1 )
                {
                   
                    var tokenstring = new Authorization().GenerateJwtToken(Configuration, user);
                    user.AccessToken = tokenstring;                
                     
                    return Ok(Helper.GenerateSuccess(Constants.Saved, user));        
                   
                  
                }

        
                else
                {
                    return BadRequest(Helper.GenerateBadRequest(Constants.Error));
                }
            
            }
            else
            {
                return BadRequest(Helper.GenerateBadRequest(Constants.Error));
            }
        

        }


        [Route("api/users")]
        [HttpGet]
        public IActionResult GetAllUsers(int take = 5,int skip = 0)
        {
            List<User> user = new List<User>();
            ListHelper listHelper = new ListHelper();
            //listHelper.query = query ?? string.Empty;
            //listHelper.sortBy = sortBy ?? string.Empty;
            listHelper.take = take > 0 ? take : 5;
            listHelper.skip = skip > 0 ? skip : 0;
            //listHelper.Filter = Filter ?? string.Empty;
            var users = userrepository.GetAllUsers(listHelper);
            if (users.users.Count > 0)
            {
                return Ok(Helper.GenerateSuccess(Constants.Success, users));
            }
            else
            {
                return BadRequest(Helper.GenerateBadRequest(Constants.UserNotFound));
            }
        }

        [AllowAnonymous]
        [Authorize]
        [Route("api/users/{id}")]
        [HttpGet]
        public IActionResult GetUserByID(int id)
        {
            if (id == Authorization.GetUserId(HttpContext))
            {

                var blog = userrepository.GetUserByID(id);
            return Ok(Helper.GenerateSuccess(Constants.Success, blog));
            }

            else
            {
                return BadRequest(Helper.GenerateBadRequest(Constants.UserNotFound));
            }

        }


        //[HttpPatch]
        //[Route("api/users/{id}")]
        //public IActionResult EditUser(int ID)
        //{

        //    if (ID == Authorization.GetUserId(HttpContext))
        //    {
        //        JsonResponse response = new JsonResponse();
        //        UserRepository userrepository = new UserRepository();


        //        if (Request.HasFormContentType)
        //        {
        //            if (Request.Form.Files != null || Request.Form.Files.Count > 0)
        //            {
        //                User user = null;
        //                user = new Model.User();
        //                user.ID = ID;

        //                var userDetail = userrepository.GetUserByID(ID);

        //                if (userDetail == null)
        //                {
        //                    return BadRequest(Helper.GenerateBadRequest(Constants.UserNotFound));
        //                }
        //                var file = Request.Form.Files[0];
        //                string path = System.IO.Path.Combine(Helper.Configuration["UserThumb"]);

        //                string imageName = user.ID.ToString().Trim() + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + ".jpg";
        //                string imgPath = System.IO.Path.Combine(path, imageName);
        //                using (var fileStream = new FileStream(imgPath, FileMode.Create))
        //                {
        //                    file.CopyTo(fileStream);
        //                }

        //                userDetail.Profile = imageName;
        //                User user11 = new User();
        //                user11.UserName = HttpContext.Request.Form["UserName"].ToString() ?? string.Empty;
        //                user11.Email = HttpContext.Request.Form["Email"].ToString() ?? string.Empty;
        //                user11.Password = HttpContext.Request.Form["Password"].ToString() ?? string.Empty;
        //                //user11.Password = Helper.HashPassword(users.Password);
        //                user11.Password = Helper.HashPassword(user11.Password);
        //                user11.Profile = imageName;
        //                user11.ID = ID;
        //                user11.Status = 1;
        //                var success = new UserRepository().EditUser(user11);
        //                return Ok(Helper.GenerateSuccess(Constants.Update));

        //            }
        //            else
        //            {
        //                return BadRequest();
        //            }
        //        }
        //        else
        //        {
        //            Stream req = Request.Body;
        //            string json = new StreamReader(req).ReadToEnd();
        //            //User user = null;
        //            User users = new User();
        //            users.UserName = HttpContext.Request.Form["UserName"].ToString() ?? string.Empty;
        //            users.Email = HttpContext.Request.Form["Email"].ToString() ?? string.Empty;
        //            users.Password = HttpContext.Request.Form["Password"].ToString() ?? string.Empty;
        //            users.ID = ID;
        //            users.Status = 1;
        //            var success = new UserRepository().EditUser(users);
        //            return Ok(Helper.GenerateSuccess(Constants.Update));


        //        }
        //    }
        //    else
        //    {
        //        return BadRequest();
        //    }
        //}


        [AllowAnonymous]
        [Authorize]
        [HttpDelete]
        [Route("api/users/{id}")]
        public IActionResult DeleteUser(int id)
        {
            var d = Request.Headers;
            if (id == Authorization.GetUserId(HttpContext))
            {
                var user = userrepository.GetUserByID(id);
                userrepository.DeleteUser(id);
                return Ok(Helper.GenerateSuccess(Constants.Success));
            }
            else
            {
                return BadRequest(Helper.GenerateBadRequest(Constants.InvalidRequest));
            }
        }

          [AllowAnonymous]
        [Authorize]

        [HttpPatch]
        [Route("api/users/{id}")]
        public IActionResult ActiveUser(int id)
        {
            var d = Request.Headers;
            if (id == Authorization.GetUserId(HttpContext))
            {
                var faq = userrepository.GetUserByID(id);
            userrepository.ActiveUser(id);
            return Ok(Helper.GenerateSuccess(Constants.Success));
            }
            else
            {
                return BadRequest(Helper.GenerateBadRequest(Constants.InvalidRequest));
            }
        }
        #endregion
    }
}
