﻿using System;
using System.Collections.Generic;
using System.IO;
using CMS_API.Model;
using CMS_API.Repository;
using CMS_API.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace CMS_API.Controllers
{
    public class TestimonialController : ControllerBase
    {
        #region Properties
        public IConfiguration Configuration { get; }

        private TestimonialRepository testimonialrepository = new TestimonialRepository();

        #endregion Properties
        #region Constructor
        public TestimonialController(IConfiguration _Configuration)
        {
            Configuration = _Configuration;
        }
        #endregion Constructor
        #region Methods

        [Route("api/testimonials")]
        [HttpPost]
        public IActionResult AddTestimonial()
        {
            Testimonial testimonial = new Testimonial();

            testimonial.Title = Helper.getFormDataItem(HttpContext, "Title");
            testimonial.Description = Helper.getFormDataItem(HttpContext, "Description");
            testimonial.Designation = Helper.getFormDataItem(HttpContext, "Designation");
            testimonial.Name = Helper.getFormDataItem(HttpContext, "Name");
            testimonial.createdBy = Helper.getFormDataItem(HttpContext, "createdBy");


            if (testimonial != null)
            {
                if (Request?.Form?.Files?.Count > 0)
                {
                    var file = Request.Form.Files[0];
                    string path = System.IO.Path.Combine(Helper.Configuration["ImagesPath"]);
                    string imageName = testimonial.Title.Trim() + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + ".jpg";
                    string imgPath = System.IO.Path.Combine(path, imageName);
                    if (file.ContentType.Contains("jpeg") || file.ContentType.Contains("jpg") || file.ContentType.Contains("png"))
                    {
                        using (var fileStream = new FileStream(imgPath, FileMode.Create))
                        {
                            file.CopyTo(fileStream);
                        }
                    }
                    else
                    {
                        return BadRequest(Helper.GenerateBadRequest(Constants.UserType));
                    }
                    testimonial.Image = imageName;
                }
                if (string.IsNullOrEmpty(testimonial.Title.Trim()))
                {
                    return BadRequest(Helper.GenerateBadRequest(Constants.UserName));
                }
                if (string.IsNullOrEmpty(testimonial.Description.Trim()))
                {
                    return BadRequest(Helper.GenerateBadRequest(Constants.UserName));
                }

                if (string.IsNullOrEmpty(testimonial.Designation.Trim()))
                {
                    return BadRequest(Helper.GenerateBadRequest(Constants.UserName));
                }
                if (string.IsNullOrEmpty(testimonial.Designation.Trim()))
                {
                    return BadRequest(Helper.GenerateBadRequest(Constants.UserName));
                }


                if (string.IsNullOrEmpty(testimonial.Name.Trim()))
                {
                    return BadRequest(Helper.GenerateBadRequest(Constants.UserName));
                }
                Testimonial testimonials = testimonialrepository.AddTestimonial(testimonial);


                return Ok(Helper.GenerateSuccess(Constants.Saved, testimonial));

            }
            else
            {
                return BadRequest(Helper.GenerateBadRequest(Constants.Error));
            }
        }

        [Route("api/testimonials")]
        [HttpGet]
        public IActionResult GetAllTestimonial([FromQuery] string query = null, [FromQuery] string sortBy = null, [FromQuery] string Filter = null, [FromQuery] int take = 5, [FromQuery] int skip = 0)

        {
            List<Testimonial> testimonial = new List<Testimonial>();
            ListHelper listHelper = new ListHelper();
            listHelper.query = query ?? string.Empty;
            listHelper.sortBy = sortBy ?? string.Empty;
            listHelper.take = take > 0 ? take : 5;
            listHelper.skip = skip > 0 ? skip : 0;
            var users = testimonialrepository.GetAllTestimonial(listHelper);
            if (users.testimonials.Count > 0)
            {
                return Ok(Helper.GenerateSuccess(Constants.Success, users));
            }
            else
            {
                return BadRequest(Helper.GenerateBadRequest(Constants.UserNotFound));
            }
        }

        [Route("api/testimonials/{id}")]
        [HttpGet]
        public IActionResult GetAllTestimonialByID(int id)
        {
            var testimonial = testimonialrepository.GetTestimonialByID(id);

            return Ok(Helper.GenerateSuccess(Constants.Success, testimonial));

        }

        [HttpDelete]
        [AllowAnonymous]
        [Authorize]
        [Route("api/testimonials/{id}")]
        public IActionResult DeleteTestimonial(int id)
        {
            if (id == Authorization.GetUserId(HttpContext))
            {
                var testimonial = testimonialrepository.GetTestimonialByID(id);
                testimonialrepository.DeleteTestimonial(id);
                return Ok(Helper.GenerateSuccess(Constants.Success));
            }
            else
            {
                return BadRequest(Helper.GenerateBadRequest(Constants.InvalidRequest));
            }

        }

        [HttpPatch]
        [Route("api/testimonials/{id}")]
        public IActionResult EditTestimonial(int id)
        {

            JsonResponse response = new JsonResponse();
            TestimonialRepository testimonialRepository = new TestimonialRepository();
            if (Request.HasFormContentType)
            {
                if (Request.Form.Files != null || Request.Form.Files.Count > 0)
                {
                    Testimonial testimonial = null;

                    testimonial = new Model.Testimonial();
                    testimonial.ID = id;
                    var userDetail = testimonialRepository.GetTestimonialByID(id);
                    if (userDetail == null)
                    {
                        return BadRequest(Helper.GenerateBadRequest(Constants.UserNotFound));
                    }
                    var file = Request.Form.Files[0];
                    string path = Path.Combine(Helper.Configuration["ImagesPath"]);

                    string imageName = testimonial.ID.ToString().Trim() + "_" + DateTime.Now.ToString("yyyyMMddHHmmssf") + ".jpg";
                    string imgPath = Path.Combine(path, imageName);
                    using (var fileStream = new FileStream(imgPath, FileMode.Create))
                    {
                        file.CopyTo(fileStream);
                    }
                    userDetail.Image = imageName;
                    Testimonial user11 = new Testimonial();
                    user11.Title = HttpContext.Request.Form["Title"].ToString() ?? string.Empty;
                    user11.Name = HttpContext.Request.Form["Name"].ToString() ?? string.Empty;
                    user11.Description = HttpContext.Request.Form["Description"].ToString() ?? string.Empty;
                    user11.Designation = HttpContext.Request.Form["Designation"].ToString() ?? string.Empty;
                    user11.createdBy = HttpContext.Request.Form["createdBy"].ToString() ?? string.Empty;
                    user11.Image = imageName;
                    user11.ID = id;
                    user11.Status = 1;
                    var success = new TestimonialRepository().EditTestimonial(user11);

                    return Ok(Helper.GenerateSuccess(Constants.Update));

                }
                else
                {
                    return BadRequest();
                }
            }
            else
            {
                Stream req = Request.Body;
                string json = new StreamReader(req).ReadToEnd();
                Testimonial users = new Testimonial();
                users.Title = HttpContext.Request.Form["Title"].ToString() ?? string.Empty;
                users.Name = HttpContext.Request.Form["Name"].ToString() ?? string.Empty;
                users.Description = HttpContext.Request.Form["Description"].ToString() ?? string.Empty;
                users.Designation = HttpContext.Request.Form["Designation"].ToString() ?? string.Empty;
                users.createdBy = HttpContext.Request.Form["createdBy"].ToString() ?? string.Empty;
                users.ID = id;
                users.Status = 1;
                var success = new TestimonialRepository().EditTestimonial(users);
                return Ok(Helper.GenerateSuccess(Constants.Update));
            }
        }
        #endregion
    }

}

