﻿using System;
using System.Collections.Generic;
using CMS_API.Model;
using CMS_API.Repository;
using CMS_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace CMS_API.Controllers
{


    public class FAQController : ControllerBase
    {
        #region Properties
        public IConfiguration Configuration { get; }

        private FAQRepository faqrepository = new FAQRepository();

        #endregion Properties
        #region Constructor
        public FAQController(IConfiguration _Configuration)
        {
            Configuration = _Configuration;
        }
        #endregion Constructor
        #region Methods
        [HttpPost]
        [Route("api/faqs")]
        public IActionResult AddFAQ()
        {
            FAQ faq = new FAQ();
            faq.Question = HttpContext.Request.Form["Question"].ToString() ?? string.Empty;
            faq.Answers = HttpContext.Request.Form["Answers"].ToString() ?? string.Empty;
            faq.createdBy = HttpContext.Request.Form["createdBy"].ToString() ?? string.Empty;


            if (faq != null)
            {
                if (string.IsNullOrEmpty(faq.Question))
                {
                    return BadRequest(Helper.GenerateBadRequest(Constants.Question));
                }
                if (string.IsNullOrEmpty(faq.Answers))
                {
                    return BadRequest(Helper.GenerateBadRequest(Constants.Answer));
                }
                if (string.IsNullOrEmpty(faq.createdBy.Trim()))
                {
                    return BadRequest(Helper.GenerateBadRequest(Constants.CreatedBy));
                }
                var result = faqrepository.AddFAQ(faq);


                return Ok(Helper.GenerateSuccess("Success", result));
            }


            else
            {
                return BadRequest(Helper.GenerateBadRequest(Constants.Error));
            }
        }

        [Route("api/faqs")]
        [HttpGet]
        public IActionResult GetAllFAQS([FromQuery] string query = null, [FromQuery] string sortBy = null, [FromQuery] string Filter = null, [FromQuery] int take = 5, [FromQuery] int skip = 0)
        {
            List<FAQ> faq = new List<FAQ>();
            ListHelper listHelper = new ListHelper();
            listHelper.query = query ?? string.Empty;
            listHelper.sortBy = sortBy ?? string.Empty;
            listHelper.take = take > 0 ? take : 5;
            listHelper.skip = skip > 0 ? skip : 0;
            var faqs = faqrepository.GetAllFAQS(listHelper);
            if (faqs.faqs.Count > 0)
            {
                return Ok(Helper.GenerateSuccess(Constants.Success, faqs));
            }
            else
            {
                return BadRequest(Helper.GenerateBadRequest(Constants.UserNotFound));
            }
        }


        [Route("api/faqs/{id}")]
        [HttpGet]
        public IActionResult GetFAQsByID(int id)
        {
            var blog = faqrepository.GetFAQByID(id);
            return Ok(Helper.GenerateSuccess(Constants.Success, blog));

        }


        [HttpDelete]
        [Route("api/faqs/{id}")]
        public IActionResult DeleteFAQ(int id)
        {
            var faq = faqrepository.GetFAQByID(id);
            faqrepository.DeleteFAQ(id);
            return Ok(Helper.GenerateSuccess(Constants.Success));

        }



        [HttpPatch]
        [Route("api/faqs/{id}")]
        public IActionResult EdiFAQ(int ID)
        {
            JsonResponse response = new JsonResponse();


            FAQ faq = null;
            faq = new Model.FAQ();
            faq.ID = ID;
            var faqdetails = faqrepository.GetFAQByID(ID);
            //Stream req = Request.Body;
            //string json = new StreamReader(req).ReadToEnd();
            FAQ faqs = new FAQ();

            faqs.Question = HttpContext.Request.Form["Question"].ToString() ?? string.Empty;
            faqs.Answers = HttpContext.Request.Form["Answers"].ToString() ?? string.Empty;
            faqs.createdBy = HttpContext.Request.Form["createdBy"].ToString() ?? string.Empty;
            faqs.ID = ID;
            var success = new FAQRepository().EditFAQ(faqs);
            return Ok(Helper.GenerateSuccess(Constants.Update));


        }
    }

    #endregion
    }





