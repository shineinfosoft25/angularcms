using CMS_API.Utility;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.IO;


namespace CMS_API.Filter
{
  public class ExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            context.ExceptionHandled = true;
            JsonResponse response = new JsonResponse();
            response.HttpStatusCode = 500;
            response.ErrorCode = 1;
            response.Message = "internal server error";
            context.Result = new ObjectResult(response);
            //logger nu folder nathi etle nai krva de folder bnavi ne ene permission apvi pdse
            var fileName = "Logger/Log.txt";
            string filePath = Path.Combine(fileName);
            File.AppendAllText(filePath, "\n========================\n Message : " + context.Exception.Message + "\n\n Exception : " + JsonConvert.SerializeObject(context.Exception) + "\n\n Time Stamp : " + DateTime.Now);

        }
    }
}
