USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[GetAllTestimonialCount_Sp]    Script Date: 11/17/2020 4:33:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[GetAllTestimonialCount_Sp] 
AS 
  BEGIN 
      SELECT Count(id) 
      FROM   testimonial 
      WHERE  status = 1 
  END 
GO

