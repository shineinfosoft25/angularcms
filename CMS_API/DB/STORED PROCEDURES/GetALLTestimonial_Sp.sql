USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[GetALLTestimonial_Sp]    Script Date: 11/17/2020 4:32:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetALLTestimonial_Sp]
@SortBy VARCHAR(50) = '',
@query VARCHAR(50) = '',
@take int=10,
@skip int =0,
@status Varchar(10)=1
AS  
BEGIN  

SELECT   

 *   
 FROM  
Testimonial WHERE  		
	
		(CASE WHEN (@status = '') THEN '' ELSE CONVERT(NVARCHAR(50), status) END) =@status   

	


	AND
	(	(CASE WHEN (@query = '') THEN '1' ELSE CONVERT(NVARCHAR(50), Title) END) LIKE (CASE WHEN (@query = '') THEN '1' ELSE CONVERT(NVARCHAR(max), '%'+@query+'%') END)
	
	OR
		(CASE WHEN (@query = '') THEN '1' ELSE CONVERT(NVARCHAR(50), createdBy) END) LIKE (CASE WHEN (@query = '') THEN '1' ELSE CONVERT(NVARCHAR(max), '%'+@query+'%') END))
	  ORDER BY
			(CASE @SortBy when 'Title' Then Title END) ASC,
			
			(CASE @SortBy when 'Id' Then ID END) ASC
	  OFFSET @skip ROWS
	  FETCH NEXT @take ROWS ONLY			 
END
GO

