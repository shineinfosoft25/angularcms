USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[AddTestimonial_sp]    Script Date: 11/17/2020 4:28:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AddTestimonial_sp]
@Title       VARCHAR(50), 
@Name        VARCHAR(50), 
@Description VARCHAR(max), 
@Image       VARCHAR(max), 
@Designation VARCHAR(50), 
@Status      INT, 
@createdBy   VARCHAR(50), 
@modifiedBy  VARCHAR(50)=NULL, 
@modifiedAt  DATETIME=NULL, 
@deletedAt   DATETIME=NULL 
AS 
  BEGIN 
      INSERT INTO testimonial 
      VALUES      ( @Title, 
                    @Name, 
                    @Description, 
                    @Image, 
                    @Designation, 
                    @Status, 
                    Getutcdate(), 
                    @createdBy, 
                    @modifiedAt, 
                    @modifiedBy, 
                    @deletedAt ) 
  END 
GO

