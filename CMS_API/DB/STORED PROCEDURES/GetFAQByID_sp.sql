USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[GetFAQByID_sp]    Script Date: 11/17/2020 4:33:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetFAQByID_sp] @ID INT 
AS 
  BEGIN 
      SELECT* 
      FROM   faq 
      WHERE  id = @ID 
  END 
GO

