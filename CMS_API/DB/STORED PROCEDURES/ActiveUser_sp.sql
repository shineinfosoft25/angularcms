USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[ActiveUser_sp]    Script Date: 11/18/2020 6:35:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[ActiveUser_sp]
@ID INT 
AS 
  BEGIN 
      UPDATE Users 
      SET    status = 1 
      WHERE  ID = @ID 
 END 
GO

