USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[GetAllTestimonialBYID_sp]    Script Date: 11/17/2020 4:32:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAllTestimonialBYID_sp] @ID INT 
AS 
  BEGIN 
      SELECT * 
      FROM   testimonial 
      WHERE  id = @ID 
  END 
GO

