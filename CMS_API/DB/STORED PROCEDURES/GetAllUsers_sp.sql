USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[GetAllUsers_sp]    Script Date: 11/18/2020 6:34:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAllUsers_sp]
@SortBy VARCHAR(50) = '',
@query VARCHAR(50) = '',
@take int=10,
@skip int =0,
@status int
AS  
BEGIN  

SELECT   

 *   
 FROM  
Users WHERE  		
	
		(CASE WHEN (@status = '') THEN '' ELSE CONVERT(NVARCHAR(50), status) END) =@status   

	


	AND
	(	(CASE WHEN (@query = '') THEN '1' ELSE CONVERT(NVARCHAR(50), UserName) END) LIKE (CASE WHEN (@query = '') THEN '1' ELSE CONVERT(NVARCHAR(max), '%'+@query+'%') END)
	
	OR
		(CASE WHEN (@query = '') THEN '1' ELSE CONVERT(NVARCHAR(50), UserType) END) LIKE (CASE WHEN (@query = '') THEN '1' ELSE CONVERT(NVARCHAR(max), '%'+@query+'%') END))
	  ORDER BY
			(CASE @SortBy when 'UserName' Then UserName END) ASC,
			
			(CASE @SortBy when 'Id' Then ID END) ASC
	  OFFSET @skip ROWS
	  FETCH NEXT @take ROWS ONLY			 
END
GO

