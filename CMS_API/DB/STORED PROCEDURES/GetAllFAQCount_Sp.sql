USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[GetAllFAQCount_Sp]    Script Date: 11/17/2020 4:32:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[GetAllFAQCount_Sp] 
AS 
  BEGIN 
      SELECT Count(id) 
      FROM   faq 
      WHERE  status = 1 
  END 
GO

