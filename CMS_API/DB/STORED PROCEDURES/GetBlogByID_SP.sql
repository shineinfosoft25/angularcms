USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[GetBlogByID_SP]    Script Date: 11/17/2020 4:33:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetBlogByID_SP] @Id INT 
AS 
  BEGIN 
      SELECT * 
      FROM   blog 
      WHERE  id = @Id 
  END 
GO

