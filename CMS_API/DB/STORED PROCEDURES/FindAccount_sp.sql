USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[FindAccount_sp]    Script Date: 11/17/2020 4:31:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[FindAccount_sp]
@Email    VARCHAR(250) = NULL, 
@UserName VARCHAR(50)=NULL 
AS 
  BEGIN 
      SELECT * 
      FROM   users 
      WHERE  email = @Email 
  END 
GO

