USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[Editblog_sp]    Script Date: 11/17/2020 4:30:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Editblog_sp] 
@Id              INT, 
@BlogTitle       VARCHAR(max), 
@Description     VARCHAR(max), 
@Image           VARCHAR(max), 
@MetaTitle       VARCHAR(max), 
@MetaDescription VARCHAR(max), 
@AuthorName      VARCHAR(max), 
@STATUS          INT 
AS 
  BEGIN 
      UPDATE blog 
      SET    blogtitle = @BlogTitle, 
             description = @Description, 
             image = @Image, 
             metatitle = @MetaTitle, 
             metadescription = @MetaDescription, 
             authorname = @AuthorName, 
             status = @STATUS 
      WHERE  id = @Id 
  END 
GO

