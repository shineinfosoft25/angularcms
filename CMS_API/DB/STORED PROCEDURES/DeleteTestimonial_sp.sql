USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[DeleteTestimonial_sp]    Script Date: 11/17/2020 4:29:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[DeleteTestimonial_sp] @ID INT 
AS 
  BEGIN 
      UPDATE testimonial 
      SET    status = '0' 
      WHERE  id = @ID 
  END 
GO

