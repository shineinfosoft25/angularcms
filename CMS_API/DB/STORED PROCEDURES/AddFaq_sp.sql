USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[AddFAQ_sp]    Script Date: 11/17/2020 4:28:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AddFAQ_sp] 
@Question  VARCHAR(max), 
@Answers   VARCHAR(max), 
@status    INT, 
@createdBy VARCHAR(50) 
AS 
  BEGIN 
      INSERT INTO faq 
      VALUES      ( @Question, 
                    @Answers, 
                    @status, 
                    @createdBy, 
                    Getdate(), 
                    Getdate() ) 
  END 
GO

