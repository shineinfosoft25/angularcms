USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[EditUser_sp]    Script Date: 11/17/2020 4:31:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EditUser_sp] 
@ID       INT, 
@UserName VARCHAR(50), 
@Email    VARCHAR(50), 
@Password VARCHAR(50), 
@Status   INT, 
@UserType VARCHAR(10), 
@Profile  VARCHAR(max) 
AS 
  BEGIN 
      UPDATE users 
      SET    username = Isnull(@UserName, username), 
             email = Isnull(@Email, email), 
             password = Isnull(@Password, password), 
             status = Isnull(@Status, status), 
             usertype = Isnull(@UserType, usertype), 
             profile = Isnull(@Profile, profile) 
      WHERE  id = @ID 
  END 
GO

