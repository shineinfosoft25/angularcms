USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[EditTestimonial_SP]    Script Date: 11/17/2020 4:30:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EditTestimonial_SP] 
@ID          INT, 
@Title       VARCHAR(50), 
@Name        VARCHAR(50), 
@Description VARCHAR(max), 
@createdBy   VARCHAR(max), 
@Image       VARCHAR(max), 
@Designation VARCHAR(50) 
AS 
  BEGIN 
      UPDATE testimonial 
      SET    title = @Title, 
             NAME = @Name, 
             description = @Description, 
             image = @Image, 
             designation = @Designation, 
             createdby = @createdBy, 
             modifiedat = Getdate() 
      WHERE  id = @ID 
  END
GO

