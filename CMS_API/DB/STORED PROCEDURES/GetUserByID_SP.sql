USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[GetUserByID_SP]    Script Date: 11/17/2020 4:33:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetUserByID_SP] @ID INT 
AS 
  BEGIN 
      SELECT * 
      FROM   users 
      WHERE  id = @ID 
  END 
GO

