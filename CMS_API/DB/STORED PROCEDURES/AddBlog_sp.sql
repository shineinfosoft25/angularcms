USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[Addblog_SP]    Script Date: 11/17/2020 4:28:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Addblog_SP] 
@BlogTitle       VARCHAR(max), 
@Description     VARCHAR(max), 
@Image           VARCHAR(max), 
@MetaTitle       VARCHAR(max), 
@MetaDescription VARCHAR(max), 
@AuthorName      VARCHAR(20), 
@STATUS          INT 
AS 
  BEGIN 
      INSERT INTO blog 
      VALUES      ( @BlogTitle, 
                    @Description, 
                    @Image, 
                    @MetaTitle, 
                    @MetaDescription, 
                    @AuthorName, 
                    Getdate(), 
                    @STATUS ) 
  END 
GO

