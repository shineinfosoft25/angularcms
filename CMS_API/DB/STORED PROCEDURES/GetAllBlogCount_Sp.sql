USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[GetAllBlogCount_Sp]    Script Date: 11/17/2020 4:31:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[GetAllBlogCount_Sp] 
AS 
  BEGIN 
      SELECT Count(id) 
      FROM   blog 
      WHERE  status = 1 
  END 
GO

