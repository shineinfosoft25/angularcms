USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[CheckEmail_sp]    Script Date: 11/17/2020 4:29:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CheckEmail_sp] @Email VARCHAR(50) 
AS 
  BEGIN 
      SELECT * 
      FROM   users 
      WHERE  email = @Email 
  END 
GO

