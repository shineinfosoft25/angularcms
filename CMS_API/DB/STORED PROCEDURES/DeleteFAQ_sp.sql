USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[DeleteFAQ_sp]    Script Date: 11/17/2020 4:29:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DeleteFAQ_sp] @ID INT 
AS 
  BEGIN 
      UPDATE faq 
      SET    status = 0 
      WHERE  id = @ID 
  END 
GO

