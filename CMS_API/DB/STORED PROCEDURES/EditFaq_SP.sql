USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[EditFaq_SP]    Script Date: 11/17/2020 4:30:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EditFaq_SP] 
@ID        INT, 
@Question  VARCHAR(50), 
@Answers   VARCHAR(50), 
@createdBy VARCHAR(50) 
AS 
  BEGIN 
      UPDATE faq 
      SET    question = @Question, 
             answers = @Answers, 
             createdby = @createdBy, 
             modifiedat = Getdate() 
      WHERE  id = @ID 
  END 
GO

