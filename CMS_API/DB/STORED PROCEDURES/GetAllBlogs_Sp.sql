USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[GetAllBlogs_Sp]    Script Date: 11/17/2020 4:32:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetAllBlogs_Sp]
@SortBy VARCHAR(50) = '',
@query VARCHAR(50) = '',
@take int=10,
@skip int =0,
@status Varchar(10)=1
AS  
BEGIN  

SELECT   
 *   
 FROM  
Blog WHERE  		
	
		(CASE WHEN (@status = '') THEN '' ELSE CONVERT(NVARCHAR(50), Status) END) =@status   

	


	AND
	(	(CASE WHEN (@query = '') THEN '1' ELSE CONVERT(NVARCHAR(50), BlogTitle) END) LIKE (CASE WHEN (@query = '') THEN '1' ELSE CONVERT(NVARCHAR(max), '%'+@query+'%') END)
	
	OR
		(CASE WHEN (@query = '') THEN '1' ELSE CONVERT(NVARCHAR(50), Description) END) LIKE (CASE WHEN (@query = '') THEN '1' ELSE CONVERT(NVARCHAR(max), '%'+@query+'%') END))
	  ORDER BY
			(CASE @SortBy when 'BlogTitle' Then BlogTitle END) ASC,
			(CASE @SortBy when 'Description' Then Description END) ASC,
			(CASE @SortBy when 'Id' Then Id END) ASC
	  OFFSET @skip ROWS
	  FETCH NEXT @take ROWS ONLY			 
END
GO

