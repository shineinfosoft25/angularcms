USE [CMS]
GO

/****** Object:  StoredProcedure [dbo].[Adduser_Sp]    Script Date: 11/17/2020 4:28:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Adduser_Sp]
@UserName VARCHAR(50), 
@Email    VARCHAR(50), 
@Password VARCHAR(50), 
@Status   INT, 
@UserType VARCHAR(10), 
@Profile  VARCHAR(max) 
AS 
  BEGIN 
      INSERT INTO users 
      VALUES      ( @UserName, 
                    @Email, 
                    @Password, 
                    @Status, 
                    @UserType, 
                    Getdate(), 
                    @Profile ) 
  END 
GO

