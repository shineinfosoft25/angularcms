USE [CMS]
GO

/****** Object:  Table [dbo].[Blog]    Script Date: 11/10/2020 4:51:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Blog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BlogTitle] [varchar](max) NULL,
	[Description] [varchar](max) NULL,
	[Image] [varchar](max) NULL,
	[MetaTitle] [varchar](max) NULL,
	[MetaDescription] [varchar](max) NULL,
	[AuthorName] [varchar](20) NULL,
	[PublishDate] [datetime] NULL,
	[STATUS] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

